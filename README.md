Description
-----------
Orientiming is a framework for accessing data from SportIdent equipment.

It was originally developed for the Sycamore Scramble A-Meet organized by Backwoods Orienteering Klub on February 20-21, 2010.

It was further improved for the Birkhead Championship Classics organized on March 10-11, 2012.

The solution ran on a laptop connected to a BSF7 download station and to a regular inkjet printer. Card data was recorded and a PDF with the splits was produced and printed. Results were calculated using another piece of software, which was downloading the punch data over HTTP.

Support for a generic print command has been added later, with an example script for printing on a Brother QL-700 thermal printer under `commands/`

Orientiming was developed in python, and tested on Linux (Fedora 25). In theory it could also run on Windows; see the Roadmap section.

Orientiming is distributed under the Eclipse Public License. See `LICENSE` or `LICENSE.html` for licensing terms.

Availability
------------
At this point, Orientiming is available as source only. Sources can be downloaded using Mercurial. To check out the sources:

    git clone https://bitbucket.org/misa/orientiming

You can also browse the sources without downloading the whole repository: http://bitbucket.org/misa/orientiming/src/

Requirements
------------
* python (tested on python 3.5 and 3.6)
* pyserial
* reportlab (for PDF generation)
* sqlite3 (for storing partial results on individual courses)
* pdftops (for converting PDF to ps - may not be necessary with recent versions of CUPS)
* python3-gstreamer1 (for sound support) Optional

Installation
------------
We recommend installing orientiming in a virtualenv.

Assuming you have checked out the source into git/orientiming:

```
#!sh
# Optional - sound support. Example is for Fedora
dnf -y install gstreamer-python gstreamer-plugins-good
mkdir ~/virtualenv
python3.6 -m venv ~/virtualenv/orientiming --system-site-packages
. ~/virtualenv/orientiming/bin/activate
cd ~/git/orientiming
pip install -e .
```

Configuration
-------------
After installation, you will need to edit `~/.otimingrc`
At the very least, you will need:

```
#!ini
[DEFAULT]
dataPath: /var/tmp/orientiming-2017/results
eventTitle: FooBar Classics
eventDate: October 10-11, 2017

[readout]
serial: /dev/ttyUSB0
soundFileDir: /home/misa/hg/orientiming/sounds
```

See `orientiming/config.py` for all available configuration options.

You may need to add your user to the `dialup` group, in order to have read/write permissions on the serial port.

Running
-------
To run the main loop:

    orientiming

Several additional scripts exist:
* `orientiming-reprint-latest` - reprints the last downloaded card
* `orientiming-reprocess-data` - reprocesses all downloads and re-generates the PDF files.

Tested Configuration
--------------------
* SportIdent BSF7, software versions: 5.59 and 5.65
* Cards tested: SI-Card5, SI-Card6, SI-Card8, SI-Card9, SI-pCard.

SI-Card6* and SI-tCard were unavailable for testing, but probably work too.

Roadmap / Intended Features
---------------------------
* Nokia N810 Internet Tablet support
* Windows Support
* Graphical UI (pygtk)
* Report missing runners (cards that have punched start control but have not punched finish control)
* Facilitate registration during a large event: associate name with Card ID
* Easier customization
    * Customize printout template
    * Customize sounds
    * Customize courses
    * With pre-assigned courses, validate run against course
    * Without pre-assigned courses, prompt user to choose their intended course when multiple possiblities are present
* On-the-spot corrections. Some of the errors we've seen during an A-Meet:
    * Start control not punched (use the start time recorded on paper)
    * Finish control not punched (use the finish time recorded on paper)
    * Card not cleared (ability to set start time and remove a set of controls that are irrelevant for that run)

Other Features
--------------
* Compute results. Overlaps with SIDResult
