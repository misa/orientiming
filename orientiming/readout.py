# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

import logging
import six
import socket
import sys

from . import config
from . import datahandler
from . import siconst as cn
from . import silib
from . import soundplayer


log = logging.getLogger(__name__)


def usage():
    log.error("Usage: %s tty [38400|4800]", sys.argv[0])


class SerialReader(silib.SerialReader):
    _blockCounts = {
        cn.SICMD_GET_SI_CARD_89P: (2, [0, 1]),
        cn.SICMD_GET_SI_CARD_5: (1, [0]),
        # This is technically not right, we should look at _cardBlocks
        cn.SICMD_GET_SI_CARD_6: (3, [0, 6, 7]),
    }

    def __init__(self, tty, cfg):
        silib.SerialReader.__init__(self, tty)
        self.cfg = cfg
        self.cfg.read()
        self.dataHandler = datahandler.DataHandler(self.cfg)
        self.soundPlayer = soundplayer.SoundPlayer()

    def run(self):
        fmt = "\r  %s           "
        chars = ['/', '-', '\\', '|']
        idx = 0
        while 1:
            sys.stdout.write(fmt % chars[idx])
            idx = (idx + 1) % 4
            sys.stdout.flush()
            try:
                self.runOnce()
            except silib.TimeoutError:
                continue

    def runOnce(self):
        # clean up gstreamer
        self.soundPlayer.playing
        try:
            cmd, params, terminator = self.readOnePacket()
        except silib.InvalidCRCError:
            self.signalFailure("Invalid CRC")
            return
        except silib.UnknownCommand:
            self.signalFailure("Unknown command")
            return

        blocks = []
        blockCount, blockIndices = self._blockCounts.get(cmd, (None, None))
        if blockCount is None:
            log.error("Unexpected command %02x", ord(cmd))
            return
        data = params[-128:]
        assert len(data) == 128
        blocks.append((0, data))

        for i in range(blockCount - 1):
            try:
                cmd, params, terminator = self.readOnePacket()
            except silib.InvalidCRCError:
                self.signalFailure("Invalid CRC")
                return
            except silib.UnknownCommand:
                self.signalFailure("Unknown command")
                return
            blockNo = params[2]
            data = params[-128:]
            assert len(data) == 128
            blocks.append((blockNo, data))

        if [x[0] for x in blocks] != blockIndices:
            self.signalFailure("Wrong data blocks were read")
            return

        filename = self.dataHandler.writeBlocks(blocks)
        card = self.dataHandler.readCardData(filename)

        self.dataHandler.processCard(card, filename)

        if self.isLoanedCard(card):
            self.signalException("Please return card to organizer")
        elif card.card_id not in self.dataHandler.namesMap:
            self.signalException("Name mismatch")
        else:
            self.signalSuccess(filename)
        self.notify()

    def isLoanedCard(self, card):
        return card.card_id in self.cfg.loanedCards

    def notify(self):
        addr = self.cfg.notificationAddress
        if not addr:
            return
        try:
            sock = socket.create_connection(addr, timeout=1)
            sock.send("Boo")
            sock.close()
        except socket.error as e:
            log.error("Error: %s", e)

    def signalSuccess(self, fpath):
        return self._playSoundFile(self.cfg.soundFileOk)

    def signalFailure(self, reason):
        self._ser.flushInput()
        return self._playSoundFile(self.cfg.soundFileError)

    def signalException(self, reason):
        self._ser.flushInput()
        return self._playSoundFile(self.cfg.soundFileException)

    def _playSoundFile(self, fpath):
        if fpath is None:
            return
        if self.soundPlayer.playing:
            return
        self.soundPlayer.play(fpath)


def main():
    level = logging.INFO
    logging.basicConfig(level=level,
                        format="%(asctime)-15s %(message)s")
    cfg = config.Configuration()
    cfg.read()
    if len(sys.argv) == 1:
        tty = cfg.serial
    else:
        tty = sys.argv[1]

    reader = SerialReader(tty, cfg)
    reader.connect()
    if not reader.connected:
        return 1

    try:
        reader.run()
    except KeyboardInterrupt:
        return 0


if __name__ == '__main__':
    sys.exit(main())
