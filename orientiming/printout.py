# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

from __future__ import unicode_literals

import logging
import os
import subprocess
import time
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer
from reportlab.platypus.tables import Table, TableStyle
from reportlab.lib import colors
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.units import inch, mm


log = logging.getLogger(__name__)


class Printout(object):
    # Page length is not important, we determine it automatically
    pageSize = (62*mm, 17*inch)
    fontSize = 9

    PrintCmd = ['/usr/bin/lp']

    def __init__(self, cfg, namesMap=None):
        self.cfg = cfg
        self.printoutPath = self.cfg.printoutPath
        if not os.path.exists(self.printoutPath):
            os.makedirs(self.printoutPath)
        self.printoutPsPath = self.printoutPath + '-ps'
        if not os.path.exists(self.printoutPsPath):
            os.makedirs(self.printoutPsPath)
        self.namesMap = namesMap or {}
        self.PrintCmd = self.cfg.printCommand

    @classmethod
    def _toTime(cls, secs):
        h = int(secs / 3600)
        m = int((secs % 3600) / 60)
        s = int(secs % 60)
        return "%02d:%02d:%02d" % (h, m, s)

    @classmethod
    def _toSplit(cls, secs):
        m = int(secs / 60)
        if secs < 0:
            # Floor division ftw
            m += 1
        s = int(abs(secs) % 60)
        ret = "%d:%02d" % (m, s)
        if secs < 0:
            ret = '-%s' % ret
        return ret

    def generateText(self, card, stream, courseDiff=None):
        stream.write("\n")
        runner = self.getRunnerName(card)
        if runner.strip():
            stream.write("  Runner:  %s\n" % runner)
        stream.write("  SI-Card: %s\n" % card.card_id)
        stream.write("\n")
        table = self.cardToTable(card, courseDiff=courseDiff)

        fmt = "%-9s %s %7s %7s\n"
        idxfmt = "%3s %3s  "
        tup = (table[0][0].center(9), table[0][2], table[0][3].center(7),
               table[0][4].center(7))
        stream.write(fmt % tup)
        for ent in table[1:-1]:
            ent = (idxfmt % (ent[0], ent[1]), ) + tuple(ent[2:])
            stream.write(fmt % ent)
        tup = (table[-1][0].center(9), table[-1][2], table[-1][3], table[-1][4])
        stream.write(fmt % tup)
        stream.write("\n")

    def _newDoc(self, destFile, pageSize=None):
        if pageSize is None:
            pageSize = self.pageSize
        doc = SimpleDocTemplate(destFile, pagesize=pageSize)
        # doc.showBoundary = True
        doc.topMargin = 5
        doc.bottomMargin = 5
        doc.leftMargin = 5
        doc.rightMargin = 5
        return doc

    def generatePdf(self, card, fileName, courseDiff=None):
        destFile = os.path.join(self.printoutPath, fileName) + '.pdf'
        # In order to create a pdf with the proper page size,
        # unfortunately we need to create it twice. Attempt to be fast
        # the first time by not writing it to the disk at all
        doc = self._newDoc('/dev/null')

        spacerHeight = 10
        Story = [Spacer(1, spacerHeight)]
        fontSize = self.fontSize
        leftIndent = 20
        style = ParagraphStyle(
            name="normal", fontSize=fontSize,
            leading=fontSize, leftIndent=leftIndent)
        titleStyle = ParagraphStyle(
            name="title", fontSize=fontSize + 3,
            leftIndent=leftIndent)
        dateStyle = ParagraphStyle(
            name="date", fontSize=fontSize + 2,
            fontName="Times-Italic", leftIndent=leftIndent)
        printDateStyle = ParagraphStyle(
            name="printDate",
            fontSize=fontSize - 2,
            fontName="Times-Italic", leftIndent=leftIndent)
        paragraphs = []
        p = Paragraph(self.cfg.eventTitle, titleStyle)
        paragraphs.append(p)
        p = Paragraph(self.cfg.eventDate, dateStyle)
        paragraphs.append(p)
        paragraphs.append(Spacer(1, spacerHeight))

        runner = self.getRunnerName(card)
        p = Paragraph(runner, style)
        paragraphs.append(p)

        cardLabel = "SI-Card: <b>%s</b>" % card.card_id
        p = Paragraph(cardLabel, style)
        paragraphs.append(p)

        startSec = card.time_start.asSeconds()
        finishSec = card.time_finish.asSeconds()

        courseStatus = {True: 'Finished', False: 'DNF'}
        if courseDiff and courseDiff.course:
            finished = (bool(courseDiff.matched) and
                        None not in [startSec, finishSec])
            cstat = courseStatus[bool(finished)]
            if finished:
                # Check for overtime
                if finishSec - startSec > 3 * 3600:
                    cstat = 'Overtime'
            label = "Course: <b>%s</b> - %s" % (
                courseDiff.course.name, cstat)
            p = Paragraph(label, style)
            paragraphs.append(p)
            paragraphs.append(Spacer(1, spacerHeight))

        Story.extend(paragraphs)
        Story.append(Spacer(1, spacerHeight))
        Story.append(self.formatData(card, courseDiff=courseDiff))
        Story.append(Spacer(1, spacerHeight))

        timeStr = time.strftime("Generated: %Y-%m-%d %H:%M:%S",
                                time.localtime(time.time()))
        p = Paragraph(timeStr, printDateStyle)
        Story.append(p)

        doc.build(Story[:])

        # Extract proper page length, by subtracting the frame height
        # from the frame's current position (reportlab uses lower left
        # corner as (0, 0), but it fills up the page top-to-bottom)
        frame = doc.frame
        pageSize = (frame._width, frame._height - frame._y + 35)
        doc = self._newDoc(destFile, pageSize)
        doc.build(Story)

    def generatePostscript(self, fileName):
        pdfFile = os.path.join(self.printoutPath, fileName) + '.pdf'
        psFile = os.path.join(self.printoutPsPath, fileName) + '.ps'
        cmd = ['/usr/bin/pdftops', pdfFile, psFile]
        p = subprocess.Popen(cmd)
        pid, sts = os.waitpid(p.pid, 0)
        if sts != 0:
            log.error("Return code: %d", sts)
            return None
        return psFile

    def getRunnerName(self, card):
        runner = self.namesMap.get(card.card_id)
        if runner is None:
            runner_str = b"%s %s" % (card.name_1, card.name_2)
        else:
            runner_str = ' '.join(runner)
        return runner_str

    def printPostscript(self, fileName):
        psFile = self.generatePostscript(fileName)
        self._print(psFile)

    def printPdf(self, fileName):
        pdfFile = os.path.join(self.printoutPath, fileName) + '.pdf'
        return self._print(pdfFile)

    def _print(self, filePath):
        cmd = self.PrintCmd[:]
        cmd.append(filePath)
        devnull = open(os.devnull, "w")
        p = subprocess.Popen(cmd, stdout=devnull, stderr=devnull)
        pid, sts = os.waitpid(p.pid, 0)
        if sts != 0:
            log.error("Return code: %d", sts)
        return sts

    def formatData(self, card, courseDiff=None):
        data = self.cardToTable(card, courseDiff=courseDiff)

        tstyle = TableStyle([
            ('TEXTCOLOR', (0, 0), (-1, -1), colors.black),
            ('FONTSIZE', (0, 0), (-1, -1), self.fontSize),
            ('TOPPADDING', (0, 0), (-1, -1), 0),
            ('BOTTOMPADDING', (0, 0), (-1, -1), 0),
            ('LEFTPADDING', (0, 0), (-1, -1), 4),
            ('RIGHTPADDING', (0, 0), (-1, -1), 4),
            ('LEADING', (0, 0), (-1, -1), self.fontSize+2),
            # Start and finish labels
            ('SPAN', (0, 0), (1, 0)),
            ('SPAN', (0, -1), (1, -1)),
            ('ALIGN', (0, 0), (1, 0), 'CENTER'),
            ('ALIGN', (0, -1), (1, -1), 'CENTER'),
            # Rest of table
            ('ALIGN', (1, 1), (-1, -1), 'RIGHT'),
            ('FONT', (1, 1), (-1, -2), 'Helvetica'),
            ('FONT', (2, -1), (-1, -1), 'Helvetica'),
            # Control index
            ('ALIGN', (0, 1), (0, -2), 'RIGHT'),
            ('FONT', (0, 1), (0, -2), 'Times-Italic'),
            # Start time
            ('ALIGN', (2, 0), (2, 0), 'RIGHT'),
            ('FONT', (2, 0), (2, 0), 'Helvetica'),
            ('LINEBELOW', (0, 0), (1, 0), 0.25, colors.black),
            ('LINEBELOW', (3, 0), (-1, 0), 0.25, colors.black),
            # Splits column
            ('LEFTPADDING', (3, 0), (3, -1), 5),
            # Finish
            ('FONT', (-1, -1), (-1, -1), 'Helvetica-BoldOblique'),
            ('LINEABOVE', (-1, -1), (-1, -1), 0.25, colors.black),
            ('LINEABOVE', (0, -1), (1, -1), 0.25, colors.black),
        ])
        t = Table(data, style=tstyle)
        return t

    @classmethod
    def cardToTable(cls, card, courseDiff=None):
        startSec = card.time_start.asSeconds()
        finishSec = card.time_finish.asSeconds()
        if startSec is None:
            # They have not punched start. Use the clear time, for lack of a
            # better option
            startSec = card.time_clear.asSeconds()
            if startSec is None:
                startSec = card.punches[0].asSeconds() if card.punches else 0
            startSecString = "NO-START"
        else:
            startSecString = cls._toTime(startSec)
        prevTime = startSec
        # Now compute splits
        splits = []
        punchAsSecs = []
        controlNos = []
        for punch in card.punches:
            punchAsSec = punch.asSeconds()
            punchAsSecs.append(punchAsSec)
            splits.append(punchAsSec - prevTime)
            prevTime = punchAsSec
            controlNos.append(punch.getControlNumber())
        if finishSec is not None:
            splits.append(finishSec - prevTime)
        else:
            splits.append(0)

        if not courseDiff:
            courseDiffList = []
        else:
            courseDiffList = courseDiff.diff
        courseDiffIter = iter(courseDiffList)
        data = [
            ['Start', '', startSecString, 'Split', 'Time'],
        ]
        idx = 0
        for (controlNo, punchAsSec, split) in zip(controlNos, punchAsSecs, splits):
            for oper in courseDiffIter:
                if oper.control == controlNo:
                    # Either a control on the course, or an extra
                    # control that was picked up
                    if oper.op == '+':
                        punchNumber = "++"
                    else:
                        assert oper.op == '*'
                        idx += 1
                        punchNumber = "%d." % idx
                    break
                assert oper.op == '-'
                data.append(['---', oper.control, '', '', ''])
                # To make the number match the control index on the course,
                # increment now
                idx += 1
            else:  # for
                # The course diff finished before consuming all
                # controls. This is a bug, or no course diff was
                # specified
                idx += 1
                punchNumber = "%d." % idx
            data.append([punchNumber, controlNo,
                         cls._toTime(punchAsSec), cls._toSplit(split),
                         cls._toSplit(punchAsSec - startSec)])
        # Add any missing controls at the end
        for oper in courseDiffIter:
            assert oper.op == '-'
            data.append(['---', oper.control, '', '', ''])
        if finishSec is None:
            finishSecString = "NO-FIN"
            lastSplitString = "XX:XX"
            totalTimeString = "XX:XX"
        else:
            finishSecString = cls._toTime(finishSec)
            if not punchAsSecs:
                # Not a single punch; there's no final punch then
                lastSplitString = "XX:XX"
            else:
                lastSplitString = cls._toSplit(finishSec - punchAsSecs[-1])
            if card.time_start.asSeconds() is None:
                totalTimeString = "XX:XX"
            else:
                totalTimeString = cls._toSplit(finishSec - startSec)
        data.append(['Finish', '', finishSecString, lastSplitString,
                     totalTimeString])
        return data
