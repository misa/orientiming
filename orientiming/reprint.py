# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

from __future__ import unicode_literals

import logging
import os
import sys

from . import config
import subprocess


log = logging.getLogger(__name__)


def main():
    cfg = config.Configuration()
    cfg.read()
    level = logging.INFO
    logging.basicConfig(level=level,
                        format="%(asctime)-15s %(message)s")

    if cfg.printAsPdf.upper() == 'TRUE':
        printoutPath = cfg.printoutPath
    else:
        printoutPath = cfg.printoutPath + '-ps'
    flist = sorted(os.listdir(printoutPath))
    if not flist:
        return
    # Grab the latest file
    lastFile = flist[-1]
    filePath = os.path.join(printoutPath, lastFile)
    cmd = cfg.printCommand + [filePath]
    p = subprocess.Popen(cmd)
    pid, sts = os.waitpid(p.pid, 0)
    if sts != 0:
        log.error("Return code: %d", sts)
    return sts


if __name__ == '__main__':
    sys.exit(main())
