# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

from __future__ import print_function
from __future__ import unicode_literals

import logging
import serial
import six
import time

from . import crc529
from . import siconst as c

log = logging.getLogger(__name__)


class BaseException(Exception):
    pass


class TimeoutError(BaseException):
    pass


class IncompleteReadError(BaseException):
    pass


class InvalidResponseError(BaseException):
    pass


class InvalidCRCError(BaseException):
    pass


class UnknownResponse(BaseException):
    pass


class UnknownCommand(BaseException):
    pass


class ConnectionError(BaseException):
    pass


class ProtocolConfigurationError(BaseException):
    pass


class SoftwareVersionError(BaseException):
    pass


class CardBlocksError(BaseException):
    pass


class RetrySameSpeed(ConnectionError):
    pass


class RetryNextSpeed(ConnectionError):
    pass


finalChars = set([c.SIPROTO_ETX, c.SIPROTO_NAK])


def genCommand(cmd, *params):
    paramsStr = b''.join(params)
    buf = cmd + six.int2byte(len(paramsStr)) + paramsStr
    buf += crc529.crc529(buf)
    return buf


def sendCommand(cmd, *params, **kwargs):
    buf = c.SIPROTO_STX + genCommand(cmd, *params) + c.SIPROTO_ETX
    if kwargs.get('wakeUp'):
        buf = c.SIPROTO_WAK + c.SIPROTO_STX + buf
    log.debug("Sending %s", hexify(buf))
    return buf


def sendResponse(cmd, *params):
    buf = c.SIPROTO_STX + genCommand(cmd, *params) + c.SIPROTO_ETX
    return buf


def readOnePacket(stream):
    # Discard any non-STX param
    cmd = None
    stxRead = False
    while 1:
        buf = stream.read(1)
        if len(buf) != 1:
            raise TimeoutError
        if buf == c.SIPROTO_STX:
            # Gobble up as many STX chars as available
            stxRead = True
            continue
        if not stxRead:
            # Ignore any non-STX char that is received before an STX
            continue
        # A non-STX char read after an STX is a command
        cmd = buf
        break
    cmdByte = ord(cmd)
    if cmdByte < 0x80 or cmdByte == 0xC4:
        raise UnknownCommand(cmd)
    paramLenChr = stream.read(1)
    if len(paramLenChr) != 1:
        raise IncompleteReadError()
    paramLen = ord(paramLenChr)
    # Now read the parameters plus the two CRC values and the protocol
    # terminator
    restLen = paramLen + 3
    data = b""
    dataLen = len(data)
    while dataLen < restLen:
        buf = stream.read(min(restLen - dataLen, 100))
        if not buf:
            break
        data += buf
        dataLen = len(data)
    if dataLen != restLen:
        raise InvalidResponseError()
    params = data[:paramLen]
    crc = data[paramLen:paramLen+2]
    terminator = six.int2byte(six.indexbytes(data, paramLen+2))
    # Verify crc
    buf = crc529.crc529(cmd + six.int2byte(len(params)) + params)
    if buf != crc:
        log.error("Data: %s", hexify(c.SIPROTO_STX + cmd + paramLenChr + data))
        raise InvalidCRCError(buf, crc)
    if terminator not in finalChars:
        raise UnknownResponse(hexify(cmd + params + terminator))
    log.debug("Received %s", hexify(cmd + paramLenChr + params + crc +
                                    terminator))
    return cmd, params, terminator


def readResponse(stream):
    return readOnePacket(stream)


def _readResponseOld(stream, cmd, firstParamChar):
    params = []
    char = firstParamChar
    while 1:
        if char in finalChars:
            return cmd, "".join(params), char
        # If the character we read is DLE, we need to read the next 2 chars
        if char == c.SIPROTO_DLE:
            toRead = 2
        else:
            toRead = 1
            params.append(char)
        buf = stream.read(toRead)
        if len(buf) != toRead:
            raise TimeoutError()
        if toRead == 2:
            if ord(buf[0]) > 0x1F:
                raise Exception("DLE introducing non-control char %02x" %
                                ord(buf[0]))
            params.append(buf[0])
        char = buf[-1]

    # Should never be reached
    return None, None, None


def setSpeed(stream, speed):
    assert(speed in [4800, 38400])
    p = (speed == 38400) and c.SIPARAM_SPEED_38400 or c.SIPARAM_SPEED_4800
    ret, rcmd, params = _sendCommand(stream, c.SICMD_SET_SPEED, p)
    if len(params) != 3:
        raise InvalidResponseError(params)
    if params[-1] != p:
        raise InvalidResponseError(params)
    return ret


def _genericGetSystemValueCommand(stream, prmOut, expectedLen=None):
    ret, rcmd, params = _sendCommand(stream, c.SICMD_GET_SYSTEM_VALUE, prmOut)
    if not ret:
        return ret, None, None
    # if rcmd == '\xC4':
    #    raise C4Exception()
    #    return True, params[0], params[1:]
    if expectedLen is not None and (len(params) != expectedLen or
                                    params[2] != prmOut[0]):
        log.error("Error: actual %d, expected %d", len(params), expectedLen)
        raise UnknownResponse(hexify(params))
    stcode = params[:2]
    svbdata = params[3:]
    return True, stcode, svbdata


def getProtocolConfiguration(stream):
    prmOut = c.SIPARAM_GET_PROTOCOL_CONFIGURATION
    return _genericGetSystemValueCommand(stream, prmOut, 4)


def getBackupMemoryPointer(stream):
    prmOut = c.SIPARAM_GET_BACKUP_MEMORY_POINTER
    return _genericGetSystemValueCommand(stream, prmOut, 0x0A)


def getSoftwareVersion(stream):
    prmOut = c.SIPARAM_GET_SOFTWARE_VERSION
    return _genericGetSystemValueCommand(stream, prmOut, 0x06)


def getCardBlocks(stream):
    prmOut = c.SIPARAM_GET_CARD_BLOCKS
    return _genericGetSystemValueCommand(stream, prmOut, 0x04)


def getAllParams(stream):
    prmOut = c.SIPARAM_GET_ALL_PARAMS
    expectedLen = ord(prmOut[-1]) + 3
    return _genericGetSystemValueCommand(stream, prmOut, expectedLen)


def getDataFromBackupMemory(stream, ptr, byteCount):
    ptrstr = intToBytes(ptr, 3)
    ret, rcmd, params = _sendCommand(stream, c.SICMD_GET_DATA_FROM_BACKUP,
                                     ptrstr, chr(byteCount))
    if not ret:
        return ret, None, None
    stcode = params[:2]
    rptrstr = params[2:5]
    data = params[5:]
    if ptrstr != rptrstr:
        raise Exception("Expected %s, got %s" %
                        (hexify(ptrstr), hexify(rptrstr)))
    return True, stcode, data


def _sendCommand(stream, cmd, *sparams):
    for i in range(10):
        if hasattr(stream, 'flushInput'):
            stream.flushInput()
        if hasattr(stream, 'flushOutput'):
            stream.flushOutput()
        stream.write(sendCommand(cmd, *sparams))
        try:
            rcmd, params, term = readResponse(stream)
        except IncompleteReadError:
            log.info("Incomplete read, retrying...")
            continue
        except TimeoutError:
            log.info("Timeout, retrying...")
            continue
        else:
            if rcmd == b'\xC4':
                log.info("C4 received, retrying... %s", hexify(params))
                time.sleep(2)
                continue
            break
    else:  # for
        raise
    if cmd != rcmd and rcmd != b'\xC4':
        raise InvalidResponseError(
            "Command sent: %02x; Command received: %02x" %
            (ord(cmd), ord(rcmd)))
    return (term == c.SIPROTO_ETX) and True or False, rcmd, params


def bytesToInt(bytestring):
    return seqToInt(bytearray(bytestring))


def seqToInt(sequence):
    val = 0
    for b in sequence:
        val = val << 8
        val += b
    return val


def intToBytes(val, minLen):
    curLen = 0
    bytes = []
    while curLen < minLen or val:
        bytes.append(chr(val & 0xFF))
        val = val >> 8
        curLen += 1
    bytes.reverse()
    return ''.join(bytes)


def addToMemPtr(ptr, val):
    intptr = bytesToInt(*ptr)
    return intToBytes(intptr + val, 3)


def hexify(data):
    if isinstance(data, six.binary_type):
        return "".join(r'\x%02x' % c for c in bytearray(data))
    return "".join(r'\x%02x' % c for c in data)


class SerialReader(object):
    # Timeout, in seconds
    _timeout = 4
    # Fallback for serial speeds
    _bauds = [38400, 4800]

    def __init__(self, tty):
        self._tty = tty
        self._ser = None
        self._softwareVersion = None
        self._cardBlocks = None
        self._stationCode = None
        self._protocolConfig = None
        self._extendedProtocol = False
        self._autoSendOut = False
        self._handshake = False
        self._accessWithPassword = False
        self._readOutAfterPunch = False

    def connect(self):
        for i in range(3):
            for baud in self._bauds:
                for j in range(3):
                    try:
                        self.connectAtSpeed(baud)
                        self.postConnect(baud)
                        return
                    except RetrySameSpeed:
                        continue
                    except RetryNextSpeed:
                        break
        else:
            log.error("Unable to connect")

    def connectAtSpeed(self, baud):
        log.info("Connecting to %s at %s...", self._tty, baud)
        ser = serial.Serial(self._tty, baud, timeout=self._timeout)

        ser.write(sendCommand(c.SICMD_SET_MS_MODE,
                              c.SIPARAM_MS_MASTER, wakeUp=True))

        try:
            cmd, params, term = readResponse(ser)
        except TimeoutError:
            ser.close()
            raise RetryNextSpeed
        except InvalidCRCError:
            ser.close()
            raise RetrySameSpeed

        if term == c.SIPROTO_ETX:
            # We've found the right speed
            self._ser = ser
            return ser
        if term == c.SIPROTO_NAK:
            # Bad response
            log.debug("NAK received; retrying")
            raise RetrySameSpeed
        raise ConnectionError("Unknown termination %02x" % term)

    @property
    def connected(self):
        return self._ser is not None

    def readOnePacket(self):
        return readOnePacket(self._ser)

    def getProtocolConfiguration(self):
        ret, stcode, dataStr = getProtocolConfiguration(self._ser)
        if not ret:
            raise ProtocolConfigurationError(ret)
        assert len(dataStr) == 1
        self._stationCode = bytesToInt(stcode)
        data = ord(dataStr)
        self._protocolConfig = data
        self._extendedProtocol = bool(data & 0x0001)
        self._autoSendOut = bool(data & 0x0002)
        self._handshake = bool(data & 0x0004)
        self._accessWithPassword = bool(data & 0x0100)
        self._readOutAfterPunch = bool(data & 0x8000)

    def getSoftwareVersion(self):
        ret, stcode, swver = getSoftwareVersion(self._ser)
        if not ret:
            raise SoftwareVersionError(ret)
        self._stationCode = bytesToInt(stcode)
        self._softwareVersion = "%s.%s%s" % tuple(swver.decode('ascii'))

    def getBackupMemoryPointer(self):
        return getBackupMemoryPointer(self._ser)

    def getCardBlocks(self):
        ret, stcode, cardBlocks = getCardBlocks(self._ser)
        if not ret:
            raise CardBlocksError(ret)
        assert len(cardBlocks) == 1
        self._cardBlocks = ord(cardBlocks)
        assert self._cardBlocks == 0xC1

    def postConnect(self, baud):
        self.resetSpeed(baud)
        for i in range(5):
            try:
                return self._postConnect(baud)
            except InvalidResponseError:
                log.debug("Invalid response, trying again...")
            except UnknownResponse:
                log.debug("Unknown response, trying again...")
        else:
            raise

    def resetSpeed(self, baud):
        if baud == 38400:
            return
        setSpeed(self._ser, 38400)
        raise RetryNextSpeed

    def _postConnect(self, baud):
        self.getProtocolConfiguration()
        self.getSoftwareVersion()
        self.getCardBlocks()
