# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

from __future__ import unicode_literals

import bisect


def longestIncreasingSubsequence(arr):
    # Length of longest increasing sequence found so far
    L = 0
    P = []
    M = []
    XM = []
    for i, xi in enumerate(arr):
        j = bisect.bisect_left(XM, xi)
        if i == 0 or j == 0:
            P.append(-1)
        else:
            P.append(M[j - 1])
        if j == L:
            M.append(i)
            XM.append(xi)
            L += 1
        elif xi < XM[j]:
            M[j] = i
            XM[j] = xi
    # Read longest increasing subsequence
    ret = []
    prevIdx = M[L - 1]
    while prevIdx != -1:
        xval = arr[prevIdx]
        ret.append(xval)
        prevIdx = P[prevIdx]
    ret.reverse()
    return ret


def longestCommonSubsequence(s1, s2):
    len1 = len(s1)
    len2 = len(s2)
    C = [([0] * (len1 + 1)) for j in range(len2 + 1)]
    for j in range(len1 + 1):
        C[0][j] = 0
    for i in range(len2 + 1):
        C[i][0] = 0
    for j in range(len1):
        for i in range(len2):
            if s1[j] == s2[i]:
                C[i+1][j+1] = 1 + C[i][j]
            else:
                C[i+1][j+1] = max(C[i][j+1], C[i+1][j])
    # Backtrace to extract the result
    i = len2
    j = len1
    ret = []
    while i > 0 and j > 0:
        if C[i][j] > C[i][j-1] and C[i][j] > C[i-1][j]:
            # We got here by appending to C[i-1][j-1]
            assert s1[j-1] == s2[i-1]
            ret.append(s1[j-1])
            i -= 1
            j -= 1
            continue
        if C[i][j] == C[i][j-1]:
            j -= 1
            continue
        i -= 1
        continue
    ret.reverse()
    return ret
