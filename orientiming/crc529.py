# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

from __future__ import print_function
from __future__ import unicode_literals
from six import indexbytes


class CRC(object):
    POLYNOM = 0x8005
    BITF = 0x8000

    @classmethod
    def crc(cls, string):
        if len(string) < 2:
            return b'\x00\x00'
        if len(string) == 2:
            return string
        # Pad with zeros; one if odd length, two (to force a crc computation
        # with a zero val at the end) for even.
        if len(string) % 2:
            string += b'\0'
        else:
            string += b'\0\0'
        crc = (indexbytes(string, 0) << 8) + indexbytes(string, 1)
        for i in range(1, len(string) // 2):
            val16 = (indexbytes(string, 2 * i) << 8) + indexbytes(string, 2 * i + 1)
            crc = cls._crc(val16, crc)

        return bytes(bytearray((crc >> 8, crc & 0xFF)))

    @classmethod
    def _crc(cls, val16, crc):
        for i in range(16):
            shouldApplyPoly = (crc & cls.BITF)
            crc <<= 1
            if val16 & cls.BITF:
                crc += 1
            if shouldApplyPoly:
                crc ^= cls.POLYNOM
            val16 <<= 1
            # 16 bits only
            crc &= 0xFFFF
        return crc


def crc529(string):
    return CRC.crc(string)
