# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

from __future__ import print_function
from __future__ import unicode_literals

import asyncio
import io
import logging
import serial_asyncio
import six
from collections import namedtuple

from . import crc529, siconst as c, model, silib

log = logging.getLogger(__name__)


class SerialReader(object):
    # Timeout, in seconds
    _timeout = 4
    # Fallback for serial speeds
    _bauds = [38400, 4800]

    def __init__(self, loop, tty):
        self.loop = loop
        self._tty = tty
        self._software_version = None
        self._card_blocks = None
        self._station_code = None
        self._protocol_config = None
        self._extended_protocol = False
        self._auto_sendout = False
        self._handshake = False
        self._access_with_password = False
        self._read_out_after_punch = False
        self._station_mode = None
        self._reader = None
        self._writer = None

    async def connect(self):
        for i in range(3):
            for baud in self._bauds:
                for j in range(3):
                    try:
                        self._reader, self._writer = await self.connect_at_speed(baud)
                        await self.post_connect(baud)
                        return await self.run_transport()
                    except silib.RetryNextSpeed:
                        break
        else:
            log.error("Unable to connect")

    async def run_transport(self):
        proto = CardHandler(self)
        transport = self._writer.transport
        # Should call transport.set_protocol but it is not defined
        transport._protocol = proto
        self.loop.call_soon(proto.connection_made, transport)
        self.loop.call_soon(transport._ensure_reader)
        return (transport, proto)

    async def connect_at_speed(self, baud):
        log.info("Connecting to %s at %s...", self._tty, baud)
        reader, writer = await serial_asyncio.open_serial_connection(
            url=self._tty, baudrate=baud)

        for i in range(3):
            try:
                cmd, params, term = await self.connect_once(reader, writer)
            except asyncio.TimeoutError:
                log.debug("Timed out")
                writer.close()
                raise silib.RetryNextSpeed
            if cmd is None:
                log.debug("Retrying")
                continue

            log.debug("Got term %r", term)
            if term == c.SIPROTO_NAK:
                # Bad response
                log.debug("NAK received; retrying")
                continue
            if term == c.SIPROTO_ETX:
                # We've found the right speed
                return reader, writer
            raise silib.ConnectionError("Unknown termination %02x" % term)

    def connect_once(self, reader, writer):
        writer.write(silib.sendCommand(c.SICMD_SET_MS_MODE,
                                       c.SIPARAM_MS_MASTER, wakeUp=True))
        return asyncio.wait_for(read_one_packet(reader), self._timeout)

    @property
    def connected(self):
        return self._writer is not None

    async def get_protocol_configuration(self):
        ret, stcode, dataStr = await self._genericGetSystemValueCommand(
            c.SIPARAM_GET_PROTOCOL_CONFIGURATION, 4)
        if not ret:
            raise silib.ProtocolConfigurationError(ret)
        assert len(dataStr) == 1
        self._station_code = silib.bytesToInt(stcode)
        data = six.byte2int(dataStr)
        self._decode_cpc(data)

    def config_summary(self):
        log.info("Configuration:")
        log.info("  Station Code: %d", self._station_code)
        log.info("  Software Version: %s", self._software_version)
        if self._station_mode:
            log.info("  Station mode: %s", self._station_mode.name)
        log.info("  Extended Protocol: %s", self._extended_protocol)
        log.info("  Auto Send Out: %s", self._auto_sendout)
        log.info("  Handshake: %s", self._handshake)
        log.info("  Access With Password: %s", self._access_with_password)
        log.info("  Readout After Punch: %s", self._read_out_after_punch)

    def _decode_cpc(self, data):
        self._protocol_config = data
        self._extended_protocol = bool(data & 0x0001)
        self._auto_sendout = bool(data & 0x0002)
        self._handshake = bool(data & 0x0004)
        self._access_with_password = bool(data & 0x0100)
        self._read_out_after_punch = bool(data & 0x8000)

    async def get_software_version(self):
        ret, stcode, swver = await self._genericGetSystemValueCommand(
            c.SIPARAM_GET_SOFTWARE_VERSION, 0x06)
        if not ret:
            raise silib.SoftwareVersionError(ret)
        self._station_code = silib.bytesToInt(stcode)
        self._software_version = "%s.%s%s" % tuple(chr(x) for x in swver)

    async def getBackupMemoryPointer(self):
        ret, stcode, swver = await self._genericGetSystemValueCommand(
            c.SIPARAM_GET_BACKUP_MEMORY_POINTER, 0x0A)
        return ret, stcode, swver

    async def get_card_blocks(self):
        ret, stcode, cardBlocks = await self._genericGetSystemValueCommand(
            c.SIPARAM_GET_CARD_BLOCKS, 0x04)
        if not ret:
            raise silib.CardBlocksError(ret)
        assert len(cardBlocks) == 1
        self._card_blocks = six.byte2int(cardBlocks)
        log.debug("Card blocks: %d", self._card_blocks)
        assert self._card_blocks == 0xC1

    async def get_info_unknown(self):
        ret, stcode, dataStr = await self._genericGetSystemValueCommand(
            c.SIPARAM_GET_INFO_UNKNOWN, 7)
        mode = six.indexbytes(dataStr, 0)
        self._station_mode = c.StationMode(mode)
        self._decode_cpc(six.indexbytes(dataStr, 3))

    async def post_connect(self, baud):
        await self.reset_speed(baud)
        for i in range(5):
            try:
                return await self._post_connect(baud)
            except silib.InvalidResponseError:
                log.debug("Invalid response, trying again...")
            except silib.UnknownResponse:
                log.debug("Unknown response, trying again...")
        else:
            raise

    async def reset_speed(self, baud):
        if baud == 38400:
            return
        await self.set_speed(38400)
        raise silib.RetryNextSpeed

    async def set_speed(self, baud):
        assert(baud in [4800, 38400])
        p = c.SIPARAM_SPEED_38400 if baud == 38400 else c.SIPARAM_SPEED_4800
        ret, rcmd, params = await self._sendCommand(c.SICMD_SET_SPEED, p)
        if len(params) != 3:
            raise silib.InvalidResponseError(params)
        if six.int2byte(params[-1]) != p:
            raise silib.InvalidResponseError(params)
        return ret

    async def _post_connect(self, baud):
        await self.get_info_unknown()
        # Sub-command 0x71 includes CPC, so don't call it again
        # await self.get_protocol_configuration()
        await self.get_software_version()
        # await self.get_card_blocks()
        self.config_summary()

    async def _genericGetSystemValueCommand(self, prmOut, expectedLen=None):
        ret, rcmd, params = await self._sendCommand(c.SICMD_GET_SYSTEM_VALUE,
                                                    prmOut)
        if not ret:
            return ret, None, None
        if expectedLen is not None and (len(params) != expectedLen or
                                        params[2] != prmOut[0]):
            log.error("Error: actual %d, expected %d", len(params), expectedLen)
            raise silib.UnknownResponse(silib.hexify(params))
        stcode = params[:2]
        svbdata = params[3:]
        return True, stcode, svbdata

    async def _sendCommand(self, cmd, *sparams):
        for i in range(10):
            self._writer.write(silib.sendCommand(cmd, *sparams))
            try:
                rcmd, params, term = await asyncio.wait_for(
                    read_one_packet(self._reader), self._timeout)
            except asyncio.TimeoutError:
                log.info("Timeout, retrying...")
                continue
            except silib.IncompleteReadError:
                log.info("Incomplete read, retrying...")
                continue
            else:
                if rcmd == b'\xC4':
                    log.info("C4 received, retrying... %s", silib.hexify(params))
                    continue
                break
        else:  # for
            raise
        if cmd != rcmd and rcmd != b'\xC4':
            raise silib.InvalidResponseError(
                "Command sent: %02x; Command received: %02x" %
                (ord(cmd), ord(rcmd)))
        return (term == c.SIPROTO_ETX), rcmd, params


async def read_one_packet(stream):
    # Discard any non-STX param
    cmd = None
    stxRead = False
    while 1:
        buf = await stream.read(1)
        log.debug("Read: %r", buf)
        if buf == c.SIPROTO_STX:
            # Gobble up as many STX chars as available
            stxRead = True
            continue
        if not stxRead:
            # Ignore any non-STX char that is received before an STX
            log.debug("Ignoring non-STX char")
            continue
        # A non-STX char read after an STX is a command
        cmd = buf
        break
    log.debug("Processing command %r", cmd)
    cmdByte = ord(cmd)
    if cmdByte < 0x80 or cmdByte == 0xC4:
        raise silib.UnknownCommand(cmd)
    paramLenChr = await stream.read(1)
    if len(paramLenChr) != 1:
        raise silib.IncompleteReadError()
    paramLen = ord(paramLenChr)
    # Now read the parameters plus the two CRC values and the protocol
    # terminator
    restLen = paramLen + 3
    data = []
    dataLen = len(data)
    while dataLen < restLen:
        buf = await stream.read(min(restLen - dataLen, 100))
        if not buf:
            break
        data.extend(bytearray(buf))
        dataLen = len(data)
    if dataLen != restLen:
        raise silib.InvalidResponseError()
    params = data[:paramLen]
    crc = bytes(data[paramLen:paramLen+2])
    terminator = six.int2byte(data[paramLen+2])
    # Verify crc
    arr = bytearray(cmd)
    arr.append(len(params))
    arr.extend(params)
    buf = crc529.crc529(bytes(arr))
    if buf != crc:
        log.error("Data: %s", silib.hexify(
            c.SIPROTO_STX + cmd + paramLenChr + bytes(data)))
        raise silib.InvalidCRCError(buf, crc)
    if terminator not in silib.finalChars:
        raise silib.UnknownResponse(silib.hexify(cmd + bytes(params) + terminator))
    log.debug("Received %s",
              silib.hexify(c.SIPROTO_STX + cmd + paramLenChr + bytes(data)))
    return cmd, params, terminator


CardCommandDef = namedtuple("CardCommandDef", "count indices")


class CardHandler(asyncio.Protocol):
    CMD_TO_CARD_DEF = {
        c.SICMD_GET_SI_CARD_89P: CardCommandDef(2, [0, 1]),
        c.SICMD_GET_SI_CARD_5: CardCommandDef(1, [0]),
        # This is technically not right, we should look at _cardBlocks
        c.SICMD_GET_SI_CARD_6: CardCommandDef(3, [0, 6, 7]),
    }

    def __init__(self, reader):
        self._si_reader = reader
        self.blocks = []

    def connection_made(self, transport):
        log.debug("Connection made")

    def data_received(self, data):
        log.debug('data received: %s', silib.hexify(data))
        cmd, params, term = self.decode_packet(data)

        card_def = self.CMD_TO_CARD_DEF.get(cmd)
        if card_def is None:
            log.error("Unexpected command %02x", ord(cmd))
            return
        payload = params[-128:]
        assert len(payload) == 128
        block_index = six.indexbytes(params, 2)
        self.blocks.append((block_index, payload))
        if [x[0] for x in self.blocks] != card_def.indices:
            log.debug("Incomplete: %s, %s",
                      [x[0] for x in self.blocks],
                      card_def.indices)
            return
        log.debug("Complete read")
        card = model.SI_CardData(b''.join(x[1] for x in self.blocks))
        # If this card has only a clear timestamp, add registration
        card_info = "{}: {} {}".format(card.card_id,
                                       card.name_1.decode('ascii'),
                                       card.name_2.decode('ascii'))
        if not (card.time_start.asSeconds() or card.time_finish.asSeconds()
                or card.record_count):
            log.info("Registration for %s", card_info)
        else:
            log.info("Finish for %s", card_info)
        self.blocks = []
        return data

    def connection_lost(self, exc):
        log.debug('port closed')
        self._si_reader.loop.stop()

    @classmethod
    def decode_packet(cls, data):
        """
        @return: cmd, params, terminator
        """
        stream = io.BytesIO(data)
        return silib.readOnePacket(stream)
