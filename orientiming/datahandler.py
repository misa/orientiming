# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

from __future__ import print_function
from __future__ import unicode_literals

import logging
import os
import tempfile
import time
import sys

from . import config
from . import courses
from . import database
from . import model
from . import printout

log = logging.getLogger(__name__)


class DataHandler(object):
    def __init__(self, cfg=None):
        if cfg is None:
            cfg = config.Configuration()
            cfg.read()
        self.cfg = cfg
        self.rawDataPath = self.cfg.rawDataPath
        if not os.path.exists(self.rawDataPath):
            os.makedirs(self.rawDataPath)
        if not os.path.exists(self.cfg.csvDataPath):
            os.makedirs(self.cfg.csvDataPath)
        self.namesMap = courses.Names(self.cfg.namesMapFile)
        self.printout = printout.Printout(self.cfg, namesMap=self.namesMap)
        self.courses = courses.Courses(self.cfg.coursesFilePath)
        self.database = database.Database(self.cfg.dbPath)
        self.idx = 1

    def writeBlocks(self, blocks):
        filename = "%.2f" % time.time()
        fpath = os.path.join(self.rawDataPath, filename)
        fTmp = tempfile.NamedTemporaryFile(dir=self.rawDataPath,
                                           prefix='.tmp', delete=False)
        try:
            for blockNo, blockData in blocks:
                fTmp.seek(blockNo * 128)
                fTmp.write(blockData)
            os.rename(fTmp.name, fpath)
        except Exception:
            os.unlink(fTmp.name)
            raise
        return filename

    def readCardData(self, filename):
        fpath = os.path.join(self.rawDataPath, filename)
        data = open(fpath, "rb").read()
        # Convert the file name to a float; we need it as download time
        downloadTime = float(filename)
        card = model.SI_CardData(data, downloadTime=downloadTime)
        if self.cfg.csvFilePrefix:
            filename = "%s-%s" % (self.cfg.csvFilePrefix, filename)
        self.writeCsvFile(card, self.cfg.csvDataPath, filename)
        return card

    def write(self, blocks):
        filename = self.writeBlocks(blocks)
        return self.readCardData(filename)

    def iterDirectoryResults(self):
        for fname in sorted(os.listdir(self.cfg.rawDataPath)):
            try:
                float(fname)
            except ValueError:
                continue
            cdata = self.readCardData(fname)
            yield fname, cdata

    def processAllRawData(self):
        idx = 0
        for fname, cdata in self.iterDirectoryResults():
            idx += 1
            cdiff = self.courses.matchPartial(cdata)
            self.printout.generatePdf(cdata, fname, courseDiff=cdiff)
            self.computeStandings(cdata, courseDiff=cdiff, idx=idx)

    def processCard(self, card, filename):
        stream = sys.stdout
        cdiff = self.courses.matchPartial(card)
        self.printout.generateText(card, stream, courseDiff=cdiff)
        self.printout.generatePdf(card, filename, courseDiff=cdiff)
        if self.cfg.printResults.upper() == 'TRUE':
            if self.cfg.printAsPdf.upper() == 'TRUE':
                self.printout.printPdf(filename)
            else:
                self.printout.printPostscript(filename)
        stream.write("\n\n")
        stream.write(self.computeStandings(card, courseDiff=cdiff,
                                           idx=self.idx))
        self.idx += 1
        stream.write("\n\n")

    def computeStandings(self, card, courseDiff=None, idx=1):
        log.info("Computing standings %s", card.card_id)
        if courseDiff is None:
            courseDiff = self.courses.matchPartial(card)
        secStart = card.time_start.asSeconds()
        secFinish = card.time_finish.asSeconds()
        is_dq = False
        if secStart is None:
            is_dq = "Disqualified: start not punched"
        if secFinish is None:
            is_dq = "Disqualified: finish not punched"
        if courseDiff.matched is None:
            return "Unable to match course"
        courseName = courseDiff.course.name
        is_dnf = not courseDiff.matched
        runTime = 0 if is_dq else secFinish - secStart
        name_1, name_2 = self.namesMap.get(card.card_id, (None, None))[:2]
        rank, totalRuns, leaderDelta = self.database.getResultRank(
            card.card_id, card.toUniqueKey(), runTime, courseName,
            data=card.toCsv(idx=idx, name_1=name_1, name_2=name_2).decode('ascii'),
            is_dnf=is_dnf, is_dq=bool(is_dq))
        self.database.commit()
        if is_dq:
            return is_dq
        if is_dnf:
            return "DNF: %s" % courseName
        return "Unofficial result on %s: %s / %s (%d:%02d after leader)" % (
            courseName, rank, totalRuns, int(leaderDelta / 60),
            int(leaderDelta % 60))

    @classmethod
    def writeCsvFile(cls, cardData, destDir, filename, downloadTime=None):
        fpath = os.path.join(destDir, filename)
        fTmp = tempfile.NamedTemporaryFile(dir=destDir,
                                           prefix='.tmp', delete=False)
        try:
            fTmp.write(cardData.toCsv(downloadTime=downloadTime))
            fTmp.write(b'\n')
            fTmp.close()
            os.rename(fTmp.name, fpath)
        except Exception:
            os.unlink(fTmp.name)
            raise


class CsvDirectoryReader(object):
    CsvHeader = (
        "No.,read at,SI-Card,St no,cat.,First name,name,club,country,"
        "sex,year-op,Email,mobile,city,street,zip,"
        "CLR_CN,CLR_DOW,Clear time,"
        "CHK_CN,CHK_DOW,Check time,"
        "ST_CN,ST_DOW,Start time,"
        "FI_CN,FI_DOW,Finish time,"
        "No. of punches,X.CN,X.DOW,X.Time,...")

    def __init__(self, dirName):
        self.dirName = dirName

    def iterResults(self):
        for fname in sorted(os.listdir(self.dirName)):
            fpath = os.path.join(self.dirName, fname)
            data = open(fpath).readline().strip()
            yield data.encode('ascii')

    def toStream(self):
        f = tempfile.TemporaryFile()
        f.write(self.CsvHeader.encode('ascii'))
        f.write(b"\n")
        for row in self.iterResults():
            f.write(row)
            f.write(b"\n")
        f.seek(0)
        return f
