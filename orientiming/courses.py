# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

from __future__ import unicode_literals

import csv
from collections import namedtuple
from . import model
from . import subsequences


class Courses(object):
    PartialMatch = namedtuple('PartialMatch', 'matched course diff')

    class Course(object):
        Operation = namedtuple('Operation', 'op control')

        def __init__(self, courseName, controls, length_km=None):
            self.name = courseName
            self.controls = controls
            self.length_km = None
            if length_km is not None:
                length_km = [x for x in length_km if x.isdigit() or x == '.']
                if length_km:
                    self.length_km = float(''.join(length_km))

        @property
        def length(self):
            return len(self.controls)

        def match(self, controls):
            # Short-circuit the LCS computation if the number of punches is
            # smaller than the number of controls in the course. This will
            # have to change when partial runs are needed.
            if len(controls) < self.length:
                return False
            subseq = subsequences.longestCommonSubsequence(self.controls,
                                                           controls)
            if subseq == self.controls:
                return True
            return False

        def matchPartial(self, controls):
            subseq = subsequences.longestCommonSubsequence(self.controls,
                                                           controls)
            diff = self.diff3(subseq, self.controls, controls)
            return subseq, diff

        @classmethod
        def diff3(cls, lcs, list1, list2):
            iter1 = iter(list1)
            iter2 = iter(list2)
            ret = []
            for item in lcs:
                # Elements from iter1 that don't match item get deleted
                for i1 in iter1:
                    if i1 == item:
                        break
                    ret.append(cls.Operation('-', i1))
                for i2 in iter2:
                    if i2 == item:
                        break
                    ret.append(cls.Operation('+', i2))
                ret.append(cls.Operation('*', item))
            # Flush the rest
            ret.extend(cls.Operation('-', i) for i in iter1)
            ret.extend(cls.Operation('+', i) for i in iter2)
            return ret

        def __gt__(self, other):
            if not isinstance(other, self.__class__):
                return True
            return (len(self.controls) > len(other.controls))

    def __init__(self, filename):
        if hasattr(filename, 'read'):
            f = filename
        else:
            f = open(filename)
        courses = []
        for line in f:
            arr = line.split('=', 2)
            if len(arr) != 3:
                continue
            label = arr[0].strip()
            controls = [int(x.strip()) for x in arr[1].split(',')]
            courses.append(self.Course(label, controls, arr[2]))
        # Sort courses by reverse length. This is an attempt to catch runs
        # that match both a longer course and a shorter course. It will not be
        # able to distinguish between courses of the same length that a runner
        # punched entirely (like A, B, C and A, D, C with runner punching
        # A, B, D, C)
        courses.sort(key=lambda x: x.length, reverse=True)
        self.courses = courses

    def match(self, controls):
        # This will not make any attempt to return a partial match. That can
        # be done too, but it was not a priority
        # Convert card punches into sequences of ints
        if isinstance(controls, model.SI_CardData):
            controls = [x.getControlNumber() for x in controls.punches]
        bestMatch = None
        for course in self.courses:
            if course.match(controls):
                if course > bestMatch:
                    bestMatch = course
        if bestMatch is None:
            return None
        return bestMatch.name

    def matchPartial(self, controls):
        """
        Return a PartialMatch
        """
        if isinstance(controls, model.SI_CardData):
            controls = [x.getControlNumber() for x in controls.punches]
        if not controls:
            return self.PartialMatch(None, None, [])
        bestMatch = None
        for course in self.courses:
            subseq, diff = course.matchPartial(controls)
            matched = (course.controls == subseq)
            if bestMatch is not None:
                # A partial match, even longer, will never overwrite
                # a shorter full match
                if bestMatch[3] and not matched:
                    continue
                # If both matches are of the same type (full or
                # partial), then the length decides
                if not (matched ^ bestMatch[3]) and \
                        len(subseq) < len(bestMatch[1]):
                    continue
            bestMatch = course, subseq, diff, matched
        if bestMatch is None:
            return self.PartialMatch(None, None, [])
        return self.PartialMatch(
            bestMatch[0].controls == bestMatch[1],
            bestMatch[0], bestMatch[2])


class Names(dict):
    def __init__(self, filename):
        if hasattr(filename, 'read'):
            stream = filename
        else:
            stream = open(filename)
        csvReader = csv.reader(stream)
        ret = {}
        for row in csvReader:
            if not row:
                continue
            if row[0].startswith('#'):
                continue
            if len(row) < 2:
                continue
            try:
                ret[int(row[0])] = row[1:3]
            except ValueError:
                continue
        self.update(ret)
