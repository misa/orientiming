# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

from __future__ import print_function
from __future__ import unicode_literals

from six.moves import configparser
import os
import shlex


class Configuration(object):
    Defaults = dict(
        dataPath='/var/lib/orientiming',
        rawDataPath='%(dataPath)s/raw',
        csvDataPath='%(dataPath)s/csv',
        csvFilePrefix='',
        eventTitle='(unset)',
        eventDate='(unset)',
        namesMapFile='%(dataPath)s/names.csv'
    )
    DefaultSection = 'DEFAULT'
    HttpSection = 'http'
    ReadoutSection = 'readout'

    def __init__(self):
        self.cp = configparser.ConfigParser(self.Defaults)
        self.cp.add_section(self.ReadoutSection)
        self.cp.set(self.ReadoutSection, 'soundFileDir',
                    '/usr/share/orientiming/sounds')
        self.cp.set(self.ReadoutSection, 'soundFileOk',
                    '%(soundFileDir)s/meepmeep.wav')
        self.cp.set(self.ReadoutSection, 'soundFileError',
                    '%(soundFileDir)s/nosir.wav')
        self.cp.set(self.ReadoutSection, 'soundFileException',
                    '%(soundFileDir)s/klaxon1.wav')
        self.cp.set(self.ReadoutSection, 'serial',
                    '/dev/ttyS0')
        self.cp.set(self.ReadoutSection, 'printoutPath',
                    '%(dataPath)s/printouts')
        self.cp.set(self.ReadoutSection, 'dbPath',
                    '%(dataPath)s/database.sqlite')
        self.cp.set(self.ReadoutSection, 'coursesFilePath',
                    '%(dataPath)s/courses.csv')
        self.cp.set(self.ReadoutSection, 'printResults',
                    'true')
        self.cp.set(self.ReadoutSection, 'printCommand',
                    '/usr/bin/lp')
        self.cp.set(self.ReadoutSection, 'printAsPdf',
                    'true')
        self.cp.set(self.ReadoutSection, 'notificationAddress',
                    '')
        self.cp.set(self.ReadoutSection, 'loanedCards',
                    '')
        self.cp.add_section(self.HttpSection)
        self.cp.set(self.HttpSection, "port", '1234')

    def read(self):
        homeDir = os.environ.get('HOME', '/root')
        self.cp.read([os.path.join(homeDir, '.otimingrc'), '.otimingrc'])

    @property
    def rawDataPath(self):
        return self.cp.get(self.DefaultSection, 'rawDataPath')

    @property
    def csvDataPath(self):
        return self.cp.get(self.DefaultSection, 'csvDataPath')

    @property
    def csvFilePrefix(self):
        return self.cp.get(self.DefaultSection, 'csvFilePrefix')

    @property
    def eventTitle(self):
        return self.cp.get(self.DefaultSection, 'eventTitle')

    @property
    def eventDate(self):
        return self.cp.get(self.DefaultSection, 'eventDate')

    @property
    def namesMapFile(self):
        return self.cp.get(self.DefaultSection, 'namesMapFile')

    @property
    def coursesFilePath(self):
        return self.cp.get(self.ReadoutSection, 'coursesFilePath')

    @property
    def printoutPath(self):
        return self.cp.get(self.ReadoutSection, 'printoutPath')

    @property
    def printCommand(self):
        return shlex.split(self.cp.get(self.ReadoutSection, 'printCommand'))

    @property
    def printAsPdf(self):
        return self.cp.get(self.ReadoutSection, 'printAsPdf')

    @property
    def httpPort(self):
        return self.cp.getint(self.HttpSection, 'port')

    @property
    def serial(self):
        return self.cp.get(self.ReadoutSection, 'serial')

    @property
    def printResults(self):
        return self.cp.get(self.ReadoutSection, 'printResults')

    @property
    def soundFileOk(self):
        fpath = self.cp.get(self.ReadoutSection, 'soundFileOk')
        if not os.path.exists(fpath):
            return None
        return fpath

    @property
    def soundFileError(self):
        fpath = self.cp.get(self.ReadoutSection, 'soundFileError')
        if not os.path.exists(fpath):
            return None
        return fpath

    @property
    def soundFileException(self):
        fpath = self.cp.get(self.ReadoutSection, 'soundFileException')
        if not os.path.exists(fpath):
            return None
        return fpath

    @property
    def notificationAddress(self):
        addr = self.cp.get(self.ReadoutSection, 'notificationAddress')
        if not addr:
            return None
        arr = addr.split(':', 1)
        if len(arr) == 1:
            return ('127.0.0.1', int(arr[0]))
        return (arr[0], int(arr[1]))

    @property
    def loanedCards(self):
        ret = set()
        loanedCards = self.cp.get(self.ReadoutSection, 'loanedCards')
        if not loanedCards:
            return ret
        # Split by ,
        loanedCards = [x.strip() for x in loanedCards.split(',')]
        # Eliminate empty entries
        loanedCards = [x for x in loanedCards if x]
        for card in loanedCards:
            if '-' not in card:
                # Individual card
                ret.add(int(card))
                continue
            arr = card.split('-', 1)
            if len(arr) != 2:
                raise Exception("Bad specification %s" % card)
            ret.update(range(int(arr[0]), int(arr[1])+1))
        return ret

    @property
    def dbPath(self):
        return self.cp.get(self.ReadoutSection, 'dbPath')

    def _getDataPath(self):
        return self.cp.get(self.DefaultSection, 'dataPath')

    def _setDataPath(self, dataPath):
        return self.cp.set(self.DefaultSection, 'dataPath', dataPath)

    dataPath = property(_getDataPath, _setDataPath)


if __name__ == '__main__':
    cfg = Configuration()
    cfg.read()
    import sys
    cfg.cp.write(sys.stdout)
