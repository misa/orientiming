# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************
from __future__ import print_function
from __future__ import unicode_literals

import sys
import sqlalchemy as sa
from collections import OrderedDict
from sqlalchemy import func, orm


class Database(object):
    def __init__(self, dbfile, eventName=None, eventStartTime=None):
        self.engine = sa.create_engine('sqlite:///%s' % dbfile)
        self.createSchema()
        self._sess = None
        if eventName is None:
            eventName = 'default'
        self.eventName = eventName
        self.eventStartTime = eventStartTime
        self._courses = {}
        self._classes = {}
        self._event = None
        self._events = {}

    def createSchema(self):
        Schema.metadata.create_all(self.engine)

    @property
    def sess(self):
        if self._sess is None:
            Schema.Session.configure(bind=self.engine)
            self._sess = Schema.Session()
        return self._sess

    def getCourse(self, courseName):
        course = self._courses.get(courseName)
        if course is not None:
            return course
        self._courses = dict(
            (x.name, x) for x in self.sess.query(Models.Course).all())
        return self._courses.get(courseName)

    def getClass(self, className):
        klass = self._classes.get(className)
        if klass is not None:
            return klass
        self._classes = dict(
            (x.name, x) for x in self.sess.query(Models.Class).all())
        return self._classes.get(className)

    def addEvent(self, eventName, startTime):
        event = self._events.get(eventName)
        if event is not None:
            return event
        self._events = dict(
            (x.name, x) for x in self.sess.query(Models.Event).all())
        event = self._events.get(eventName)
        if event is not None:
            return event
        event = Models.Event(name=eventName, startTime=startTime)
        self.sess.add(event)
        return event

    @property
    def event(self):
        if self._event is None:
            self._event = self.addEvent(self.eventName, self.eventStartTime)
        return self._event

    def addCourse(self, courseName):
        course = self.getCourse(courseName)
        if course is None:
            course = Models.Course(courseName)
            self.sess.add(course)
        return course

    def addCourses(self, courses):
        if not courses:
            return
        self._courses = dict(
            (x.name, x) for x in self.sess.query(Models.Course).all())
        missingCourses = OrderedDict((x, None)
                                     for x in courses
                                     if x not in self._courses)
        self.sess.add_all(Models.Course(x) for x in missingCourses)

    def addClasses(self, classes):
        if not classes:
            return
        self._classes = dict(
            (x.name, x) for x in self.sess.query(Models.Class).all())
        missingClasses = dict(
            (x.name, x) for x in classes if x.name not in self._classes)
        self.addCourses(x.course.name for x in missingClasses.values())
        for k in missingClasses.values():
            k.course = self.getCourse(k.course.name)
        self.sess.add_all(missingClasses.values())

    def addEntries(self, entries):
        self.addClasses([x.klass for x in entries if x.class_id is not None])
        self.addCourses([x.course for x in entries if x.course_id is not None])
        ev = self.event
        for entry in entries:
            if entry.klass is not None:
                entry.klass = self.getClass(entry.klass.name)
                entry.course = entry.course_id = None
            elif entry.course is not None:
                entry.course = self.getCourse(entry.course.name)
                entry.klass = entry.class_id = None
            if entry.event is None:
                entry.event = ev
        self.sess.add_all(entries)

    def addResult(self, cardno, hashed_id, runTime, courseName, score=None,
                  data=None, is_dnf=False, is_dq=False):
        event = self.addEvent(self.eventName, self.eventStartTime)
        sess = self.sess
        Result = Models.Result
        EventEntry = Models.EventEntry

        query = sess.query(EventEntry).filter(EventEntry.cardno == cardno)
        entry = query.first()
        if entry is None:
            course = self.addCourse(courseName)
            entry = EventEntry(event=event, cardno=cardno, name=hashed_id,
                               course=course)
            sess.add(entry)

        query = sess.query(Result).filter(Result.eventEntry == entry)
        result = query.first()
        if result is None:
            result = Result(eventEntry=entry, runTime=runTime, score=score,
                            data=data, dnf=is_dnf, dq=is_dq)
        else:
            result.data = data
            result.runTime = runTime
            result.score = score
            result.dnf = is_dnf
            result.dq = is_dq
        sess.add(result)
        return result

    def getResultRank(self, cardno, name, runTime, courseName, score=None,
                      data=None, is_dnf=False, is_dq=False):
        self.addEvent(self.eventName, self.eventStartTime)
        sess = self.sess
        Result = Models.Result
        EventEntry = Models.EventEntry
        Course = Models.Course
        Class = Models.Class

        result = self.addResult(cardno, name, runTime, courseName,
                                score=score, data=data,
                                is_dnf=is_dnf, is_dq=is_dq)
        if result.dnf or result.dq:
            return None, None, None
        if result.eventEntry.class_id is not None:
            classId = result.eventEntry.class_id
            rank = sess.query(Result).join(EventEntry).join(Class).filter(
                    Class.class_id == classId,
                    Result.dnf.is_(False),
                    Result.dq.is_(False),
                    Result.runTime < runTime).count() + 1
            totalRuns = sess.query(Result).join(EventEntry).join(Class).filter(
                    Class.class_id == classId).count()
            minTime = sess.query(
                func.min(Result.runTime)).join(EventEntry).join(Class).filter(
                    Result.dnf.is_(False),
                    Result.dq.is_(False),
                    Class.class_id == classId).scalar()
        else:
            courseId = result.eventEntry.course_id
            rank = sess.query(Result).join(EventEntry).join(Course).filter(
                Result.dnf.is_(False),
                Result.dq.is_(False),
                Course.course_id == courseId,
                Result.runTime < runTime).count() + 1
            totalRuns = sess.query(Result).join(EventEntry).join(
                Course).filter(Course.course_id == courseId).count()
            minTime = sess.query(func.min(Result.runTime)).join(
                EventEntry).join(Course).filter(
                    Result.dnf.is_(False),
                    Result.dq.is_(False),
                    Course.course_id == courseId).scalar()
        return rank, totalRuns, runTime - minTime

    def iterCourses(self):
        return self.sess.query(Models.Course).join(Models.EventEntry).join(
            Models.Event).filter(
            Models.Event.name == self.eventName,
            Models.Event.start_time == self.eventStartTime)

    def iterResultsForCourse(self, courseName):
        return self.sess.query(Models.Result).join(Models.EventEntry).join(
            Models.Course).filter(
            Models.Course.name == courseName)

    def commit(self):
        self.sess.commit()

    def rollback(self):
        self.sess.rollback()


class Models(object):
    class Event(object):
        def __init__(self, name, startTime):
            self.name = name
            self.startTime = startTime

    class Course(object):
        def __init__(self, name):
            self.name = name

    class Class(object):
        def __init__(self, name, course):
            self.name = name
            self.course = course

    class EventEntry(object):
        def __init__(self, cardno, name, event, klass=None, course=None):
            self.cardno = cardno
            self.name = name
            self.event = event
            self.klass = klass
            self.course = course

    class Result(object):
        def __init__(self, eventEntry, runTime, score=None,
                     data=None, dnf=False, dq=False):
            self.eventEntry = eventEntry
            self.runTime = runTime
            self.score = score
            self.dnf = dnf
            self.dq = dq
            self.data = data


class Schema(object):
    """
    A meet is composed of several events (first day, second day etc).

    A competitor belongs to a class, like M-21, and a class runs a specific
    course, for instance M-21 runs Red.

    A competitor is issued an identifier (SportIdent puncher ID). They may
    have different identifiers for different events, although generally
    there should be only one.

    """

    metadata = sa.MetaData()
    Session = orm.sessionmaker()

    eventsTable = sa.Table(
        'Events', metadata,
        sa.Column('event_id', sa.Integer,
                  sa.Sequence('Event_id_seq', optional=True),
                  primary_key=True),
        sa.Column('name', sa.String, nullable=False,
                  unique=True, index=True),
        sa.Column('start_time', sa.DateTime, nullable=True,
                  unique=True, index=True),
        sa.UniqueConstraint('name', 'start_time', name="event__name_start_time"),
        )

    coursesTable = sa.Table(
        'Courses', metadata,
        sa.Column('course_id', sa.Integer,
                  sa.Sequence('Courses_id_seq', optional=True),
                  primary_key=True),
        sa.Column('name', sa.String, nullable=False,
                  unique=True, index=True),
        )

    classesTable = sa.Table(
        'Classes', metadata,
        sa.Column('class_id', sa.Integer,
                  sa.Sequence('Class_id_seq', optional=True),
                  primary_key=True),
        sa.Column('name', sa.String, nullable=False,
                  unique=True, index=True),
        sa.Column('course_id',
                  sa.ForeignKey(coursesTable.c.course_id), nullable=False),
        )

    eventEntryTable = sa.Table(
        'EventEntries', metadata,
        sa.Column('event_entry_id', sa.Integer,
                  sa.Sequence('EventEntries_id_seq', optional=True),
                  primary_key=True),
        # If only one event, no need to define it
        sa.Column('event_id',
                  sa.ForeignKey(eventsTable.c.event_id), nullable=True),
        sa.Column('cardno', sa.String, nullable=False),
        sa.Column('name', sa.String, nullable=False),
        # exactly one of class and course must be defined.
        sa.Column('class_id',
                  sa.ForeignKey(classesTable.c.class_id), nullable=True),
        sa.Column('course_id',
                  sa.ForeignKey(coursesTable.c.course_id), nullable=True),
        )
    sa.Index('EventEntries_cardno_eventid_idx',
             eventEntryTable.c.cardno, eventEntryTable.c.event_id,
             unique=True)
    sa.Index('EventEntries_eventid_cardno_idx',
             eventEntryTable.c.event_id, eventEntryTable.c.cardno)
    sa.Index('EventEntries_name_eventid_idx',
             eventEntryTable.c.name, eventEntryTable.c.event_id,
             unique=True)
    sa.Index('EventEntries_eventid_name_idx',
             eventEntryTable.c.event_id, eventEntryTable.c.name)

    resultsTable = sa.Table(
        'Results', metadata,
        sa.Column('result_id', sa.Integer,
                  sa.Sequence('Results_id_seq', optional=True),
                  primary_key=True),
        sa.Column('event_entry_id',
                  sa.ForeignKey(eventEntryTable.c.event_entry_id),
                  nullable=False),
        sa.Column('run_time', sa.Integer, nullable=False),
        sa.Column('data', sa.String, nullable=True),
        sa.Column('score', sa.Integer, nullable=True),
        sa.Column('dnf', sa.Boolean, nullable=False, default=False),
        sa.Column('dq', sa.Boolean, nullable=False, default=False),
        )

    orm.mapper(Models.Event, eventsTable)
    orm.mapper(Models.Course, coursesTable)
    orm.mapper(Models.Class, classesTable, properties=dict(
        course=orm.relation(Models.Course,
                            backref=orm.backref('classes', cascade="merge"),
                            lazy=False),
        ))
    orm.mapper(Models.EventEntry, eventEntryTable, properties=dict(
        event=orm.relation(
            Models.Event,
            backref=orm.backref('eventEntries', cascade="merge"),
            lazy=False),
        klass=orm.relation(
            Models.Class,
            backref=orm.backref('eventEntries', cascade="merge"),
            lazy=False),
        course=orm.relation(
            Models.Course,
            backref=orm.backref('eventEntries', cascade="merge"),
            lazy=False),
        ))
    orm.mapper(Models.Result, resultsTable, properties=dict(
        eventEntry=orm.relation(
            Models.EventEntry,
            backref=orm.backref('eventEntries'),
            single_parent=True,
            lazy=False,
            cascade="all, delete, delete-orphan",
            ),
        runTime=resultsTable.c.run_time,
        ))


def main():
    d = Database("/tmp/x.sqlite")
    d.addCourses(["White", "Yellow", "Orange", "Brown", "Green", "Red"])
    print(d.getResultRank(12345, "John", 100, "White"))
    print(d.getResultRank(12346, "Mary", 110, "White"))
    print(d.getResultRank(12343, "Bob", 120, "White"))
    print(d.getResultRank(12340, "Violet", 130, "White"))
    print(d.getResultRank(12344, "Scarlet", 140, "White"))
    d.commit()


if __name__ == '__main__':
    sys.exit(main())
