# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

import attr
import itertools
import html
import sys
from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response

from .. import config
from .. import model
from .. import datahandler


class Frontend:
    def __init__(self):
        cfg = config.Configuration()
        cfg.read()
        self.datahandler = datahandler.DataHandler(cfg)

    def view_results(self, request):
        output = []
        output.append(html_results_head)
        for course_obj in reversed(self.datahandler.courses.courses):
            cr = CourseResults(course_obj)
            cr.extend(self.datahandler.database.iterResultsForCourse(course_obj.name))
            cr.compute()
            output.extend(cr.display_ranks())
        output.append("</div></body></html>")
        return Response('\n'.join(output), content_type="text/html")

    def view_splits(self, request):
        output = []
        output.append(html_splits_head)
        for course_obj in reversed(self.datahandler.courses.courses):
            cr = CourseResults(course_obj)
            cr.extend(self.datahandler.database.iterResultsForCourse(course_obj.name))
            cr.compute()
            output.extend(cr.display_splits())
        output.append("</div></body></html>")
        return Response('\n'.join(output), content_type="text/html")


class CourseResults(object):
    @attr.s
    class Leg(object):
        seconds = attr.ib(default=None)
        cumulative = attr.ib(default=None)
        index = attr.ib(default=None)
        offset = attr.ib(default=None)
        cumulative_offset = attr.ib(default=None)
        cumulative_rank = attr.ib(default=None)

    @attr.s
    class Result(object):
        row = attr.ib()
        card = attr.ib()
        leg_times = attr.ib(default=[])

    def __init__(self, course):
        self.course = course
        self.results_valid = []
        self.results_dnf = []
        self.results_dq = []

    def add(self, result):
        r = self.Result(row=result,
                        card=model.SI_Card.from_csv(result.data))
        self.fill_leg_times(r)

        if r.row.dnf:
            self.results_dnf.append(r)
        elif r.row.dq:
            self.results_dq.append(r)
        else:
            self.results_valid.append(r)
        return self

    def extend(self, results):
        for r in results:
            self.add(r)
        return self

    def compute(self):
        self.results_valid.sort(
            key=lambda x: (x.row.runTime, -len(x.card.punches)))
        self.results_dnf.sort(key=lambda x: -len(x.card.punches))
        self.results_dq.sort(key=lambda x: -len(x.card.punches))
        self.compute_comparative_times(itertools.chain(
            self.results_valid, self.results_dnf, self.results_dq))

    def compute_comparative_times(self, results):
        results = list(results)
        if not results:
            return
        ideal = []
        for leg_idx in range(len(self.course.controls)+1):
            valid_results = [
                x for x in results
                if len(x.leg_times) > leg_idx  # this leg is not DNF
                and x.leg_times[leg_idx].seconds is not None]

            # Figure out best leg time
            valid_results.sort(key=lambda x: x.leg_times[leg_idx].seconds)
            best_time = None
            for idx, res in enumerate(valid_results):
                leg = res.leg_times[leg_idx]
                leg.index = idx + 1
                if best_time is None:
                    # best leg here
                    best_time = leg.seconds
                    ideal.append(best_time)
                leg.offset = leg.seconds - best_time

            valid_results = [
                x for x in results
                if len(x.leg_times) > leg_idx  # this leg is not DNF
                and x.leg_times[leg_idx].cumulative is not None]
            if not valid_results:
                return
            # Figure out cumulative rank
            valid_results.sort(key=lambda x: x.leg_times[leg_idx].cumulative)
            best_time = valid_results[0].leg_times[leg_idx].cumulative
            for idx, res in enumerate(valid_results):
                leg = res.leg_times[leg_idx]
                leg.cumulative_rank = idx + 1
                leg.cumulative_offset = leg.cumulative - best_time

    def display_ranks(self):
        output = []
        H = html.escape
        COL = '<col class="{}">'.format
        output.append("<table id='{}'>".format(H(self.course.name)))
        course_length = self.course.length_km
        output.extend([
            "<colgroup>",
            COL("otiming-idx"),
            COL("otiming-cardid"),
            COL("otiming-name"),
            COL("otiming-time"),
            COL("otiming-speed"),
            COL("otiming-controls"),
            "</colgroup>",
            "<thead>",
            "<tr>",
            '<td colspan="2">{}</td>'.format(H(self.course.name)),
            '<td colspan="2">{:.1f}km</td>'.format(course_length),
            '<td colspan="2">{} controls</td>'.format(self.course.length),
            "</tr>",
            "</thead>",
            "</tbody>",
        ])
        for idx, result in enumerate(self.results_valid):
            output.extend(self._render_result(course_length, idx + 1, result))
        output.extend(
            itertools.chain(*(self._render_result(0, 0, x) for x in self.results_dnf)))
        output.extend(
            itertools.chain(*(self._render_result(0, 0, x) for x in self.results_dq)))
        output.append("</tbody>")
        output.append("</table>")
        return output

    def display_splits(self):
        output = []
        H = html.escape
        COL = '<col class="{}">'.format
        TD = '<td>{}</td>'.format
        TDSPAN = '<td colspan="{}">{}</td>'.format
        COLWIDTH = '<col class="{}" width="{}">'.format
        output.append("<table id='{}-splits'>".format(H(self.course.name)))
        course_length = self.course.length_km
        output.extend([
            "<colgroup>",
            COL("otiming-idx"),
            COL("otiming-name"),
            COL("otiming-time"),
            COL("otiming-speed"),
            COL("otiming-start"),
        ])
        for idx in range(self.course.length):
            output.extend([
                COLWIDTH("otiming-ctrl-time", "30px"),
                COLWIDTH("otiming-ctrl-delta", "30px"),
                COLWIDTH("otiming-ctrl-idx", "10px"),
            ])
        output.extend([
            COLWIDTH("otiming-finish-time", "30px"),
            COLWIDTH("otiming-finish-delta", "30px"),
            COLWIDTH("otiming-finish-idx", "10px"),
            "</colgroup>",
            "<thead>",
            '<tr class="head1">',
            '<td colspan="2">{}</td>'.format(H(self.course.name)),
            '<td colspan="4">{:.1f}km {} CP</td>'.format(course_length,
                                                         self.course.length),
            '<td colspan="{}"></td>'.format(8 + 3 * self.course.length - 6),
            "</tr>",
            '<tr class="head2">',
            TD("Pl"),
            TD("Name"),
            TD("Time"),
            TD("Start"),
        ])
        for idx, ctrl in enumerate(self.course.controls):
            output.append(TDSPAN(3, "{} ({})".format(idx + 1, ctrl)))
        output.append(TDSPAN(3, "Finish"))
        output.extend([
            "</tr>",
        ])
        output.extend([
            "</thead>",
            "</tbody>",
        ])
        for idx, result in enumerate(self.results_valid):
            output.extend(self._render_split(idx + 1, result))
        output.extend(
            itertools.chain(*(self._render_split(0, x) for x in self.results_dnf)))
        output.extend(
            itertools.chain(*(self._render_split(0, x) for x in self.results_dq)))
        output.append("</tbody>")
        output.append("</table>")
        return output

    @classmethod
    def _render_result(cls, course_length, idx, result):
        H = html.escape
        TD = '<td>{}</td>'.format
        ret = ["<tr>"]
        idx_label = idx

        if result.row.dnf or result.row.dq:
            idx_label = '--'
            if result.row.dnf:
                formatted_time = 'DNF'
            else:
                formatted_time = 'DQ'
        else:
            formatted_time = cls.as_time_string(result.row.runTime)
        ret.extend([
            TD(idx_label),
            TD(result.card.card_id),
            TD(H("{} {}".format(result.card.name_1, result.card.name_2))),
            TD(formatted_time),
        ])
        if course_length:
            ret.append(TD("{:.2f} min/km".format(
                result.row.runTime / 60.0 / course_length)))
        else:
            ret.append(TD(""))

        ret.append(TD(' '.join(str(x.control_no) for x in result.card.punches)))
        ret.append("</tr>")
        return ret

    @classmethod
    def _render_split(cls, idx, result):
        ret = ["<tr>"]
        H = html.escape
        TDRS = '<td rowspan="{}">{}</td>'.format
        if result.row.dnf or result.row.dq:
            if result.row.dnf:
                idx_label = 'DNF'
                formatted_time = cls.as_time_string(result.row.runTime)
            else:
                idx_label = 'DQ'
                formatted_time = '--'
        else:
            idx_label = idx
            formatted_time = cls.as_time_string(result.row.runTime)
        if result.card.control_start.seconds:
            formatted_start = cls.as_time_string(
                result.card.control_start.seconds, with_hour=True)
        else:
            formatted_start = '--'

        ret.extend([
            TDRS(2, idx_label),
            TDRS(2, H("{} {}".format(result.card.name_1, result.card.name_2))),
            TDRS(2, formatted_time),
            TDRS(2, formatted_start),
        ])
        for leg in result.leg_times:
            ret.extend(cls._render_split_leg_1(leg))
        ret.append("</tr>")
        ret.append("<tr>")
        for leg in result.leg_times:
            ret.extend(cls._render_split_leg_2(leg))
        return ret

    @classmethod
    def _render_split_leg_1(cls, leg):
        TD = '<td>{}</td>'.format
        if leg.cumulative_offset is not None:
            leg_diff = cls.as_time_string(leg.cumulative_offset)
            if leg.offset > 0:
                leg_diff = '+' + leg_diff
        else:
            leg_diff = ""
        ret = [
            TD('--' if leg.cumulative is None
               else cls.as_time_string(leg.cumulative)),
            TD(leg_diff),
            TD('--' if leg.cumulative_rank is None else
               '({})'.format(leg.cumulative_rank)),
        ]
        return ret

    @classmethod
    def _render_split_leg_2(cls, leg):
        TD = '<td>{}</td>'.format
        if leg.offset is None:
            leg_diff = ""
        else:
            leg_diff = cls.as_time_string(leg.offset)
            if leg.offset > 0:
                leg_diff = '+' + leg_diff
        ret = [
            TD('--' if leg.seconds is None
               else cls.as_time_string(leg.seconds)),
            TD(leg_diff),
            TD('--' if leg.cumulative_rank is None else
               '({})'.format(leg.index)),
        ]
        return ret

    @classmethod
    def as_time_string(cls, seconds, with_hour=False):
        minutes = int(seconds / 60)
        secs = abs(seconds) % 60
        if with_hour:
            hour = int(minutes / 60)
            minutes = minutes % 60
            return "{:02d}:{:02d}:{:02d}".format(hour, minutes, secs)
        return "{:d}:{:02d}".format(minutes, secs)

    def fill_leg_times(self, result):
        result.leg_times = ret = []
        time_prev = result.card.control_start.seconds
        result_controls = iter(result.card.punches)
        cumulative = 0
        for control_no in self.course.controls:
            while 1:
                try:
                    punch = next(result_controls)
                except StopIteration:
                    return ret
                if punch.control_no == control_no:
                    break
            else:
                # Unable to match the whole course
                return ret
            if time_prev is None:
                # DQ, no start time; first leg has no meaningful time
                assert len(ret) == 0
                leg = self.Leg()
            else:
                leg = self.Leg(punch.seconds - time_prev)
                cumulative += leg.seconds
                # cumulative only makes sense if there was a start time
                if result.card.control_start.seconds is not None:
                    leg.cumulative = cumulative
            time_prev = punch.seconds
            ret.append(leg)
        # Last leg, from last control to finish
        if result.card.control_finish.seconds is None:
            # DQ, no finish time
            leg = self.Leg(None)
        else:
            leg = self.Leg(result.card.control_finish.seconds - time_prev)
            if result.card.control_start.seconds is not None:
                leg.cumulative = cumulative + leg.seconds
        ret.append(leg)


def main():
    frontend = Frontend()

    config = Configurator()
    config.add_route('results', '/results')
    config.add_route('splits', '/splits')
    config.add_view(frontend.view_results, route_name='results')
    config.add_view(frontend.view_splits, route_name='splits')
    app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 8080, app)
    server.serve_forever()


html_results_head = """
<head>
  <meta http-equiv="refresh" content="10">
  <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
  <style type='text/css'>
    table {width:100%; table-layout:fixed; border-spacing: 0px; page-break-inside:avoid; }
    thead {text-align:center; vertical-align:top; background-color:#7eafcf; color:#000; font-size:21px; font-weight:bold}
    thead td:first-child { text-align:left;}
    #otiming-results th,td {border: 1px solid black; padding: 2px;}
    col.otiming-idx {width: 5%;}
    col.otiming-cardid {width: 10%;}
    col.otiming-name {}
    col.otiming-time {width: 10%;}
    col.otiming-speed {width: 10%;}
    col.otiming-controls {width: 30%;}
    tr:nth-child(even) { background-color: #ddd; }
  </style>
  <title></title>
</head>
<body>
<div id="otiming-results">
"""

html_splits_head = """
<head>
  <meta http-equiv="refresh" content="10">
  <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
  <style type='text/css'>
    table {width:100%; table-layout:auto; border-spacing: 0px; page-break-inside:avoid; }
    thead {text-align:center; vertical-align:top; background-color:#7eafcf; color:#000; font-size:21px; font-weight:bold}
    thead td:first-child { text-align:left;}
    #otiming-results th,td {border: 1px solid black; padding: 2px;}
    col.otiming-idx {width: 5%;}
    col.otiming-cardid {width: 10%;}
    col.otiming-name {}
    col.otiming-time {width: 10%;}
    col.otiming-speed {width: 10%;}
    col.otiming-controls {width: 30%;}
    tr:nth-child(even) { background-color: #ddd; }
  </style>
  <title></title>
</head>
<body>
<div id="otiming-splits">
"""


if __name__ == '__main__':
    sys.exit(main())
