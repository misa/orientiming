# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

from __future__ import print_function
from __future__ import unicode_literals

import attr
import csv
import logging
import math
import struct
import time
import six

from . import silib

log = logging.getLogger(__name__)


# Map from C data types to struct characters
class BaseScalar(object):
    pass


class uint8(BaseScalar):
    format = "B"
    length = 1


class uint16(BaseScalar):
    format = "H"
    length = 2


class uint32(BaseScalar):
    format = "I"
    length = 4


class String(object):
    def __init__(self, length):
        self.length = length
        self.format = "%ss" % self.length


class Array(object):
    __slots = ['itemType', 'itemCount']

    def __init__(self, itemType, count):
        self.itemType = itemType
        self.itemCount = count

        self.length = count * itemType.length
        self.format = "%ss" % self.length

    def unpack(self, data):
        if hasattr(self.itemType, 'unpack'):
            formatStr = "%ss" % self.itemType.length
        else:
            formatStr = self.itemType.format
        subdata = struct.unpack(formatStr * self.itemCount, data)
        if hasattr(self.itemType, 'unpack'):
            return [self.itemType.unpack(x) for x in subdata]
        return subdata


class _Meta(type):
    @property
    def length(cls):
        length = 0
        for fieldName, fieldType in cls.fields:
            length += fieldType.length
        return length

    @property
    def format(cls):
        return "%ss" % cls.length


@six.add_metaclass(_Meta)
class BasePacket(object):
    fields = ()

    @classmethod
    def unpack(cls, data):
        formatStr = "".join(x.format for (y, x) in cls.fields)
        subdata = struct.unpack(formatStr, data)
        # Now walk all fields and unpack further
        inst = cls()
        for (fieldName, fieldType), value in zip(cls.fields, subdata):
            if hasattr(fieldType, 'unpack'):
                value = fieldType.unpack(value)
            setattr(inst, fieldName, value)
        return inst

    def asTimeString(self):
        secs = self.asSeconds()
        if secs is None:
            return b''
        h = int(secs / 3600)
        m = int((secs % 3600) / 60)
        s = int(secs % 60)
        return b"%02d:%02d:%02d" % (h, m, s)


class Time12h(BasePacket):
    fields = (
        ('pth', uint8),
        ('ptl', uint8),
    )

    def asSeconds(self):
        if self.pth == 0xEE:
            return None
        return silib.seqToInt((self.pth, self.ptl))

    def getDayOfWeek(self):
        return b''


class PunchingRecordTimeOnly(Time12h):
    def toCsv(self):
        return b",,%s" % self.asTimeString()


class BasePunchingRecord(BasePacket):
    TIME12H_MAX = 12 * 3600

    def toCsv(self):
        cn = self.getControlNumber()
        return b'%s,%s,%s' % (to_bytes(cn), self.getDayOfWeek(),
                              self.asTimeString())


class SI_PunchingRecordCN(BasePunchingRecord):
    fields = (
        ('cn', uint8),
    )

    def asSeconds(self):
        return None

    def getControlNumber(self):
        return self.cn

    def getDayOfWeek(self):
        return b''


class SI_PunchingRecord3(SI_PunchingRecordCN):
    fields = (
        ('cn', uint8),
        ('time', Time12h),
    )

    def asSeconds(self):
        return self.time.asSeconds()


class SI_PunchingRecord4(BasePunchingRecord):
    fields = (
        ('ptd', uint8),
        ('cn', uint8),
        ('time', Time12h),
    )
    SubsecondEnabled = False
    DOW = [b'Su', b'Mo', b'Tu', b'We', b'Th', b'Fr', b'Sa']

    def asSeconds(self):
        ret = self.time.asSeconds()
        if ret is None:
            return None
        if self.ptd & 1:
            ret += self.TIME12H_MAX
        return ret

    def getControlNumber(self):
        if self.asSeconds() is None:
            return b''
        if self.SubsecondEnabled:
            return self.cn
        return silib.seqToInt(((self.ptd >> 6) & 3, self.cn))

    def getDayOfWeek(self):
        if self.time.pth == 0xEE:
            return b''
        dow = (self.ptd >> 1) & 7
        if dow == 7:
            log.error("Invalid day of week `8'")
            return b''
        return self.DOW[dow]


class SI_PunchingRecord4MS(SI_PunchingRecord4):
    SubsecondEnabled = True


class SI_PunchingRecord8(SI_PunchingRecord4):
    fields = (
        ('cn', uint8),
        ('std1', uint8),
        ('std0', uint8),
        ('date1', uint8),
        ('date0', uint8),
        ('time', Time12h),
        ('ms', uint8),
    )

    def getControlNumber(self):
        return self.cn


class BaseCard(BasePacket):
    @classmethod
    def identifyCard(cls, data):
        # Read enough blocks to let us decide what kind of card we have
        newCls = cls.getCardClass(data)
        if newCls is None:
            return None
        if len(data) != newCls.length:
            log.error(
                "Insufficient data to identify packet: expected %d, got %d",
                newCls.length, len(data))
            return None
        return newCls.unpack(data)

    @classmethod
    def sniffData(cls, data):
        sniff = data[:8 * 4]
        cdata = cls.unpack(sniff + b'\0' * (cls.length - 32))
        return cdata

    @classmethod
    def getCardClass(cls, data):
        cdata = cls.sniffData(data)
        if cdata.type_id != cls.TYPE_ID:
            return None
        return cls

    # Hack so we can call the original function from a sublcass
    _getCardClass = getCardClass

    NON_ASCII = bytes(bytearray(range(129, 256)))

    @classmethod
    def cleanseString(cls, s):
        return s.strip(cls.NON_ASCII).strip()

    def getRecordCount(self):
        return self.record_count

    def getCountry(self):
        return b''

    def getClub(self):
        return b''

    def getSex(self):
        return b''


class SI_Card_5(BaseCard):
    fields = (
        # 8-byte block 0
        ('reserved', uint8),
        ('ci6_4', Array(uint8, 3)),
        ('cn1', uint8),
        ('cn0', uint8),
        ('cnx', uint8),  # spreadsheet calls this CNS too, they seem to be equal  # noqa
        ('unknown_7_15', Array(uint8, 9)),

        # 8-byte block 2
        ('sb1', uint8),  # Security byte 1
        ('sn', Array(uint8, 2)),  # Lower 2 bytes of the card id, prepend cns to this  # noqa
        ('start', PunchingRecordTimeOnly),
        ('finish_time', PunchingRecordTimeOnly),
        ('record_count', uint8),
        ('sb2', uint8),  # Security byte 2
        ('check', PunchingRecordTimeOnly),
        ('sw', uint8),
        ('cns', uint8),  # Card number series
        ('cs', uint8),  # Checksum
        ('type_id', String(2)),  # 00 07

        # 8-byte block 4
        ('punch31', SI_PunchingRecordCN),
        ('punch_01_05', Array(SI_PunchingRecord3, 5)),

        # 8-byte block 6
        ('punch32', SI_PunchingRecordCN),
        ('punch_06_10', Array(SI_PunchingRecord3, 5)),

        # 8-byte block 8
        ('punch33', SI_PunchingRecordCN),
        ('punch_11_15', Array(SI_PunchingRecord3, 5)),

        # 8-byte block 10
        ('punch34', SI_PunchingRecordCN),
        ('punch_16_20', Array(SI_PunchingRecord3, 5)),

        # 8-byte block 12
        ('punch35', SI_PunchingRecordCN),
        ('punch_21_25', Array(SI_PunchingRecord3, 5)),

        # 8-byte block 14
        ('punch36', SI_PunchingRecordCN),
        ('punch_26_30', Array(SI_PunchingRecord3, 5)),
    )

    TYPE_ID = b'\x00\x07'

    @property
    def si_id(self):
        # Wacky
        if self.cns == 1:
            # First generation card should not prepend 1
            return [silib.seqToInt([self.sn[0], self.sn[1]])]
        # Everything else should pad with zeros up to a length of 5,
        # then prepend the generation
        srepr = "%d%05d" % (self.cns, silib.seqToInt([self.sn[0], self.sn[1]]))
        return [int(srepr)]

    @property
    def start_number(self):
        return silib.seqToInt([self.sn[0], self.sn[1]])

    @property
    def csv_name(self):
        return b','

    @property
    def clear(self):
        return self.check

    @property
    def finish(self):
        return self.fixTime(self.finish_time)

    def fixTime(self, timeRecord):
        """
        Compare this time record with the start time, and if it appears to be
        lower, assume that we crossed the noon boundary
        """
        secsStart = self.start.asSeconds()
        if secsStart is None:
            secsStart = self.check.asSeconds()
        secs = timeRecord.asSeconds()
        if secs is not None and secsStart is not None and secsStart > secs:
            # Assume we crossed the noon boundary
            tf = timeRecord.asSeconds() + BasePunchingRecord.TIME12H_MAX
            timeRecord = timeRecord.__class__()
            timeRecord.pth = tf >> 8
            timeRecord.ptl = tf & 0xFF
        return timeRecord

    def fixPunch(self, punch):
        """
        Fix punch record
        """
        secs = punch.asSeconds()
        if secs is None:
            return punch
        punch.time = self.fixTime(punch.time)
        return punch

    def getPunches(self):
        # We add punch31..punch36 as punches with no time
        punchingArrays = [
            self.punch_01_05, self.punch_06_10,
            self.punch_11_15, self.punch_16_20, self.punch_21_25,
            self.punch_26_30,
            [self.punch31, self.punch32, self.punch33, self.punch34,
             self.punch35, self.punch36]]
        ret = []
        idx = 0
        recordCount = self.getRecordCount()
        for arr in punchingArrays:
            if idx == recordCount:
                break
            ret.extend(self.fixPunch(x) for x in arr[:recordCount - idx])
            idx = len(ret)
        return ret

    def getRecordCount(self):
        return max(0, self.record_count - 1)


class SI_Card_6(BaseCard):
    fields = (
        # 8-byte block 0
        ('data_structure', Array(uint8, 4)),
        ('type_id', String(4)),  # EDh EDh EDh EDh
        ('control_card_type_id', uint8),  # 55h
        ('pptr', uint8),  # AAh
        ('si_id', Array(uint8, 4)),
        ('crc', Array(uint8, 2)),

        # 8-byte block 2

        ('last_control_hl', Array(uint8, 2)),
        ('record_count', uint8),  # punching pointer
        ('record_count_plus1', uint8),
        ('finish', SI_PunchingRecord4MS),
        ('start', SI_PunchingRecord4MS),
        ('check', SI_PunchingRecord4),

        # 8-byte block 4
        ('clear', SI_PunchingRecord4),
        ('last_control', SI_PunchingRecord4),
        ('start_no', Array(uint8, 4)),
        ('reserved_44_47', Array(uint8, 4)),

        # 8-byte block 6
        ('last_name', String(20)),
        ('first_name', String(20)),
        ('country', String(4)),
        ('club', String(36)),

        # 8-byte block 16
        ('user_id', String(16)),
        ('mobile_phone', String(16)),
        ('email', String(36)),
        ('street', String(20)),
        ('city', String(16)),
        ('zip', String(8)),
        ('sex', String(4)),
        ('dob', String(8)),
        ('date_of_prod', Array(uint8, 4)),

        # 8-byte block 32
        ('punching_records1', Array(SI_PunchingRecord4, 128)),
        ('punching_records0', Array(SI_PunchingRecord4, 64)),
    )

    TYPE_ID = b'\xED\xED\xED\xED'

    @property
    def start_number(self):
        return silib.seqToInt(self.start_no)

    def getCountry(self):
        return self.country.strip()

    def getClub(self):
        return self.club.strip()

    def getSex(self):
        return self.sex.strip().strip(b'\x00')

    @property
    def csv_name(self):
        return b",".join(self.cleanseString(x)
                         for x in [self.first_name, self.last_name])

    def getPunches(self):
        punchingArrays = [self.punching_records0, self.punching_records1]
        ret = []
        idx = 0
        recordCount = self.getRecordCount()
        for arr in punchingArrays:
            if idx == recordCount:
                break
            ret.extend(arr[:recordCount - idx])
            idx = len(ret)
        return ret


class SI_Card_NewHeader(BaseCard):
    fields = (
        # 8-byte block 0
        ('uid', Array(uint8, 4)),
        ('type_id', String(4)),  # EAh EAh EAh EAh

        # 8-byte block 1
        ('clear', SI_PunchingRecord4),
        ('start', SI_PunchingRecord4MS),

        # 8-byte block 2
        ('finish', SI_PunchingRecord4MS),
        ('cnh', uint8),
        ('cnl', uint8),
        ('record_count', uint8),
        ('system2', uint8),

        # 8-byte block 3
        # si3 bit3-bit0: card series
        #  02: SI-Card8
        #  01: SI-Card9
        #  04: SI-pCard
        #  06: SI-tCard
        ('si3', uint8),
        ('si_id', Array(uint8, 3)),
        ('valid_until_month', uint8),
        ('valid_until_year', uint8),
        ('system1', uint8),
        ('system0', uint8),
    )


class SI_Card_89pt(SI_Card_NewHeader):
    TYPE_ID = b'\xEA\xEA\xEA\xEA'

    # Map from a subtype id to the class.
    SubtypeClassMap = dict()

    @property
    def start_number(self):
        return b''

    @property
    def check(self):
        return self.clear

    def getPunches(self):
        return self.punching_records[:self.record_count]

    def getCardSubtype(self):
        return self.si3 & 0x7

    @classmethod
    def getCardClass(cls, data):
        if cls._getCardClass(data) is None:
            return None
        # Check subtype
        crd = cls.sniffData(data)
        newCls = cls.SubtypeClassMap.get(crd.getCardSubtype(), None)

        if newCls is None:
            log.error("Unable to decode new card")
            return None
        return newCls

    @property
    def csv_name(self):
        arr = self.name.split(b';', 2)
        return b",".join(self.cleanseString(x) for x in arr[:2])


class SI_Card_8(SI_Card_89pt):
    fields = SI_Card_NewHeader.fields + (
        # 8-byte block 4
        ('name', String(26 * 4)),

        # 8-byte block 17
        ('punching_records', Array(SI_PunchingRecord4, 30)),
    )
    SUBTYPE_ID = 2


class SI_Card_9(SI_Card_89pt):
    fields = SI_Card_NewHeader.fields + (
        # 8-byte block 4
        ('name', String(6 * 4)),

        # 8-byte block 7
        ('punching_records', Array(SI_PunchingRecord4, 50)),
    )

    SUBTYPE_ID = 1


class SI_pCard(SI_Card_89pt):
    fields = SI_Card_NewHeader.fields + (
        # 8-byte block 4
        ('name', String(32 * 4)),

        # 8-byte block 20
        ('bonus', Array(uint8, 4 * 4)),

        # 8-byte block 22
        ('punching_records', Array(SI_PunchingRecord4, 20)),
    )

    SUBTYPE_ID = 4

    def getSex(self):
        arr = self.name.split(b';')
        if len(arr) < 3:
            return ''
        return arr[2].strip()

    def getCountry(self):
        arr = self.name.split(b';')
        if len(arr) < 11:
            return ''
        return arr[10].strip()

    def getClub(self):
        arr = self.name.split(b';')
        if len(arr) < 5:
            return ''
        return arr[4].strip()


class SI_tCard(SI_Card_89pt):
    fields = SI_Card_NewHeader.fields + (
        # 8-byte block 4
        ('name', String(6 * 4)),

        # 8-byte block 7
        ('punching_records', Array(SI_PunchingRecord8, 25)),
    )

    SUBTYPE_ID = 6


for cls in [SI_Card_8, SI_Card_9, SI_pCard, SI_tCard]:
    SI_Card_89pt.SubtypeClassMap[cls.SUBTYPE_ID] = cls


class SI_CardData(object):
    __slots__ = [
        'card_id', 'name_1', 'name_2', 'time_start', 'time_finish',
        'time_clear', 'time_check', 'record_count', 'punches',
        'start_number', 'country', 'club', 'sex',
        '_crd', 'download_time']
    CardClasses = [SI_Card_89pt, SI_Card_6, SI_Card_5]

    def __init__(self, data, downloadTime=None):
        for s in self.__slots__:
            setattr(self, s, None)

        self._crd = self.identifyCard(data)
        self.card_id = silib.seqToInt(self._crd.si_id)
        self.start_number = self._crd.start_number
        self.name_1, self.name_2 = self._crd.csv_name.split(b',', 1)
        self.country = self._crd.getCountry()
        self.sex = self._crd.getSex()
        self.club = self._crd.getClub()
        self.time_clear = self._crd.clear
        self.time_check = self._crd.check
        self.time_start = self._crd.start
        self.time_finish = self._crd.finish
        self.record_count = self._crd.getRecordCount()
        self.punches = self._crd.getPunches()
        self.download_time = downloadTime

    @classmethod
    def identifyCard(cls, data):
        for cardCls in cls.CardClasses:
            card = cardCls.identifyCard(data)
            if card is not None:
                return card
        raise Exception("XXX")

    def toCsv(self, downloadTime=None, idx=1, name_1=None, name_2=None):
        # No.,read at,SI-Card,St no,cat.,First name,name,club,country,
        # sex,year-op,Email,mobile,city,street,zip,
        # CLR_CN,CLR_DOW,clear time,
        # CHK_CN,CHK_DOW, check time,
        # ST_CN,ST_DOW,start time,
        # FI_CN,FI_DOW,Finish time,
        # No. of punches,X.CN,X.DOW,X.Time,...
        if downloadTime is None:
            downloadTime = self.download_time or time.time()
        if isinstance(name_1, str):
            name_1 = name_1.encode('ascii')
        if isinstance(name_2, str):
            name_2 = name_2.encode('ascii')
        arr = [to_bytes(idx), self.formatDateToString(downloadTime),
               to_bytes(self.card_id), to_bytes(self.start_number), b'',
               name_1 or self.name_1, name_2 or self.name_2, self.club, self.country,
               self.sex, b'', b'', b'', b'', b'', b'',
               self.time_clear.toCsv(),
               self.time_check.toCsv(),
               self.time_start.toCsv(),
               self.time_finish.toCsv(),
               to_bytes(self.record_count),
               ]
        arr.extend(x.toCsv() for x in self.punches)
        return b','.join(arr)

    def toUniqueKey(self):
        secTimeClear = self.time_clear.asSeconds() or b''
        secTimeStart = self.time_start.asSeconds() or b''
        secTimeFinish = self.time_finish.asSeconds() or b''
        arr = [to_bytes(self.card_id), self.name_1, self.name_2,
               to_bytes(secTimeClear), to_bytes(secTimeStart),
               to_bytes(secTimeFinish)]
        return b','.join(arr)

    @classmethod
    def formatDateToString(cls, timeval):
        dltimestr = time.strftime("%m/%d/%Y %H:%M:%S", time.localtime(timeval))
        csec = math.trunc(math.modf(timeval)[0] * 100)
        return to_bytes("%s.%d" % (dltimestr, csec))

    def as_SI_card(self, idx=1):
        row = self.toCsv(idx=1).decode("ascii")
        return SI_Card.from_csv(row)


@attr.s(slots=True)
class SI_Card(object):
    """Decoded card"""
    idx = attr.ib()
    download_time = attr.ib()
    card_id = attr.ib()
    start_number = attr.ib()
    category = attr.ib()
    name_1 = attr.ib(default='')
    name_2 = attr.ib(default='')
    club = attr.ib(default='')
    country = attr.ib(default='')
    sex = attr.ib(default='')
    year_op = attr.ib(default='')
    email = attr.ib(default='')
    mobile = attr.ib(default='')
    city = attr.ib(default='')
    street = attr.ib(default='')
    zip = attr.ib(default='')
    control_clear = attr.ib(default=None)
    control_check = attr.ib(default=None)
    control_start = attr.ib(default=None)
    control_finish = attr.ib(default=None)
    punches = attr.ib(default=[])

    @attr.s(slots=True)
    class Control(object):
        control_no = attr.ib()
        dow = attr.ib()
        seconds = attr.ib()

        @classmethod
        def from_array(cls, arr):
            assert len(arr) == 3
            obj = cls(*arr)
            if obj.control_no:
                obj.control_no = int(obj.control_no)
            else:
                obj.control_no = None
            if obj.seconds == '':
                obj.seconds = None
            else:
                arr = obj.seconds.split(':')
                seconds = 0
                for component in arr:
                    seconds = 60 * seconds + int(component)
                obj.seconds = seconds
            return obj

        def to_array(self):
            control_no = self.control_no if self.control_no else ''
            return [control_no, self.dow, self.as_time_string()]

        def as_time_string(self):
            if self.seconds is None:
                return ''
            secs = self.seconds
            h = int(secs / 3600)
            m = int((secs % 3600) / 60)
            s = int(secs % 60)
            return "%02d:%02d:%02d" % (h, m, s)

    @classmethod
    def from_csv(cls, data):
        if isinstance(data, bytes):
            data = data.decode("ascii")
        reader = csv.reader([data])
        arr = next(reader)
        return cls.from_array(arr)

    @classmethod
    def from_array(cls, arr):
        assert len(arr) >= 29
        punch_count = int(arr[28])
        assert len(arr) >= 29 + 3 * punch_count

        card = cls(*arr[:16])
        card.card_id = int(card.card_id)
        ctrls = ['clear', 'check', 'start', 'finish']
        for idx, ctrl in enumerate(ctrls):
            field_name = 'control_{}'.format(ctrl)
            # Offset 3 fields, 14 from the beginning
            pos = 16 + 3 * idx
            field_val = cls.Control.from_array(arr[pos:pos+3])
            setattr(card, field_name, field_val)
        punches = card.punches = []
        for idx in range(punch_count):
            pos = 29 + 3 * idx
            punches.append(cls.Control.from_array(arr[pos:pos+3]))
        return card

    def to_array(self):
        arr = []
        fields = attr.fields(self.__class__)
        for field in fields[:16]:
            val = getattr(self, field.name)
            arr.append(val)
        ctrls = ['clear', 'check', 'start', 'finish']
        for ctrl in ctrls:
            val = getattr(self, 'control_{}'.format(ctrl))
            arr.extend(val.to_array())
        arr.append(len(self.punches))
        for punch in self.punches:
            arr.extend(punch.to_array())
        return arr

    def toUniqueKey(self):
        secTimeClear = self.time_clear.asSeconds() or b''
        secTimeStart = self.time_start.asSeconds() or b''
        secTimeFinish = self.time_finish.asSeconds() or b''
        arr = [to_bytes(self.card_id), self.name_1, self.name_2,
               to_bytes(secTimeClear), to_bytes(secTimeStart),
               to_bytes(secTimeFinish)]
        return b','.join(arr)


def to_bytes(obj):
    if isinstance(obj, six.binary_type):
        return obj
    if isinstance(obj, six.integer_types):
        return b'%d' % obj
    return six.b(obj)
