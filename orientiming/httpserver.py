# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

from __future__ import unicode_literals

import sys
import BaseHTTPServer

from . import config
from . import datahandler


class Runner(object):
    serverClass = None
    requestHandlerClass = None

    @classmethod
    def run(cls):
        cfg = config.Configuration()
        cfg.read()
        serverPort = cfg.httpPort
        serverAddress = ('', serverPort)
        s = cls.serverClass(serverAddress, cls.requestHandlerClass)
        s.cfg = cfg
        try:
            s.serve_forever()
        except KeyboardInterrupt:
            return 0


class HTTPServer(BaseHTTPServer.HTTPServer):
    allow_reuse_address = True


class HTTPRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_GET(self):
        # Re-read configuration, in case it changed on us
        self.server.cfg.read()
        csvDir = self.server.cfg.csvDataPath
        dirReader = datahandler.CsvDirectoryReader(csvDir)
        stream = dirReader.toStream()
        stream.seek(0, 2)
        contentLength = stream.tell()
        stream.seek(0)

        self.send_response(200)
        self.send_header("Content-Type", "text/csv")
        self.send_header("Content-Length", int(contentLength))
        self.end_headers()
        while 1:
            buf = stream.read(4096)
            if not buf:
                break
            self.wfile.write(buf)


Runner.serverClass = HTTPServer
Runner.requestHandlerClass = HTTPRequestHandler

if __name__ == '__main__':
    sys.exit(Runner.run())
