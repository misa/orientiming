# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

from __future__ import unicode_literals

try:
    import gi
except ImportError:
    gi = None
import logging
import os
import sys

log = logging.getLogger(__name__)


class SoundPlayer(object):
    def __init__(self):
        self.Gst = None
        if gi is None:
            return
        try:
            gi.require_version('Gst', '1.0')
        except ValueError:
            return
        from gi.repository import Gst
        self.Gst = Gst
        self.Gst.init(None)
        self.player = Gst.ElementFactory.make("playbin", "player")
        fakesink = Gst.ElementFactory.make("fakesink", "fakesink")
        self.player.set_property("video-sink", fakesink)
        bus = self.player.get_bus()
        bus.add_signal_watch()
        bus.enable_sync_message_emission()
        bus.connect("message", self.on_message)

    def play(self, filename):
        if self.Gst is None:
            return
        self.waitForPreviousPlay()
        self.player.set_property('uri', 'file://%s' % filename)
        self.player.set_state(self.Gst.State.PLAYING)

    @property
    def playing(self):
        if self.Gst is None:
            return
        if self.player.get_state(0)[1] == self.Gst.State.NULL:
            return False
        bus = self.player.get_bus()
        bus.poll(self.Gst.MessageType.EOS | self.Gst.MessageType.ERROR, .01)
        return self.player.get_state(0)[1] != self.Gst.State.NULL

    def waitForPreviousPlay(self):
        bus = self.player.get_bus()
        # Wait for the previous play to finish
        while self.player.get_state(0)[1] == self.Gst.State.PLAYING:
            # Polling the buss will dispatch all messages too
            bus.poll(self.Gst.MessageType.EOS | self.Gst.MessageType.ERROR, .5)

    def on_message(self, bus, message):
        t = message.type
        if t == self.Gst.MessageType.EOS:
            self.player.set_state(self.Gst.State.NULL)
        elif t == self.Gst.MessageType.ERROR:
            self.player.set_state(self.Gst.State.NULL)
            err, debug = message.parse_error()
            log.error("Error: %s, %s", err, debug)


def main():
    logging.basicConfig(level=logging.INFO, format="%(asctime)s %(name)s %(message)s")
    if len(sys.argv) < 2:
        log.error("Usage: %s <sound-file>", sys.argv[0])
        return 1
    filename = sys.argv[1]
    if not os.path.exists(filename):
        log.error("File %s not found", filename)
        return 2

    import time
    s = SoundPlayer()
    s.play(filename)
    while s.playing:
        time.sleep(0.5)


if __name__ == '__main__':
    sys.exit(main())
