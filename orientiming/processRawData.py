# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

from __future__ import unicode_literals

import sys

from . import config
from . import datahandler


def main():
    cfg = config.Configuration()
    cfg.read()

    d = datahandler.DataHandler(cfg)
    d.processAllRawData()


if __name__ == '__main__':
    sys.exit(main())
