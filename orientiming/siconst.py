# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

from __future__ import print_function
from __future__ import unicode_literals

import enum


SIPROTO_WAK = b'\xFF'
SIPROTO_STX = b'\x02'
SIPROTO_ETX = b'\x03'
SIPROTO_ACK = b'\x06'
SIPROTO_NAK = b'\x15'
SIPROTO_DLE = b'\x10'

SIPROTO_ETXNAK = bytes(bytearray((0x03 | 0x15, )))

SICMD_SET_MS_MODE = b'\xF0'
SICMD_GET_SYSTEM_VALUE = b'\x83'
SICMD_SET_SPEED = b'\xFE'
SICMD_GET_DATA_FROM_BACKUP = b'\x81'

SICMD_GET_SI_CARD_5 = b'\xB1'
SICMD_GET_SI_CARD_6 = b'\xE1'
SICMD_GET_SI_CARD_89P = b'\xEF'
SICMD_RESET_BACKUP_MEMORY_POINTER = b'\xF5'
SICMD_SET_TIME = b'\xF6'
SICMD_GET_TIME = b'\xF7'
SICMD_TURN_OFF = b'\xF8'

SIPARAM_MS_MASTER = b'\x4D'
SIPARAM_MS_SLAVE = b'\x53'
SIPARAM_SPEED_4800 = b'\x00'
SIPARAM_SPEED_38400 = b'\x01'

SIPARAM_GET_PROTOCOL_CONFIGURATION = b'\x74\x01'
SIPARAM_GET_BACKUP_MEMORY_POINTER = b'\x1C\x07'
SIPARAM_GET_SOFTWARE_VERSION = b'\x05\x03'
SIPARAM_GET_ALL_PARAMS = b'\x00\x80'
SIPARAM_GET_CARD_BLOCKS = b'\x33\x01'
SIPARAM_GET_INFO_UNKNOWN = b'\x71\x04'


class StationMode(enum.Enum):
    CONTROL = 0x02
    START = 0x03
    FINISH = 0x04
    READOUT = 0x05
    CLEAR = 0x07
    CHECK = 0x0A


SIPARAM_STATION_MODE_CONTROL = 0x02
SIPARAM_STATION_MODE_START = 0x03
SIPARAM_STATION_MODE_FINISH = 0x04
SIPARAM_STATION_MODE_READOUT = 0x05
SIPARAM_STATION_MODE_CLEAR = 0x07
SIPARAM_STATION_MODE_CHECK = 0x0A

SIPARAM_NULL = b'\x00'
