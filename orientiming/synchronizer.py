# ******************************************************************************
# Copyright (c) 2012 Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial implementation
# ******************************************************************************

from __future__ import unicode_literals

import logging
import os
import socket
import sys
import SocketServer
import subprocess

log = logging.getLogger(__name__)


class RequestHandler(SocketServer.StreamRequestHandler):
    def __init__(self, request, client_address, server):
        cmd = ['/usr/bin/rsync']
        # Add a trailing / so we always copy dir contents
        src = server.src.rstrip('/') + '/'
        # At least recent versions of rsync are smart enough to determine if
        # ssh is needed
        cmd.extend(['-a', src, server.dst])
        if os.path.exists(server.dst):
            subprocess.call(cmd)
        if server.dstport:
            try:
                sock = socket.create_connection(("localhost", server.dstport),
                                                timeout=1)
                sock.send("Boo")
                sock.close()
            except socket.error as e:
                log.exception("Error")
        log.info("Client addresss: %s", client_address)


class Server(SocketServer.TCPServer):
    allow_reuse_address = True

    def __init__(self, *args, **kwargs):
        SocketServer.TCPServer.__init__(self, *args, **kwargs)
        self.src = None
        self.dst = None
        self.dstport = None


def main():
    logging.basicConfig(level=logging.INFO, format="%(asctime)s %(name)s %(message)s")
    if len(sys.argv) < 4:
        log.error("Usage: %s <port> <srcdir> <dstdir> [<dstport>]", sys.argv[0])
        return 1

    port = int(sys.argv[1])
    server = Server(('', port), RequestHandler)
    server.src = sys.argv[2]
    server.dst = sys.argv[3]
    log.info("Running rsync %s %s", server.src, server.dst)
    if len(sys.argv) == 5:
        server.dstport = int(sys.argv[4])
        log.info("Notifying on port %d", server.dstport)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        server.server_close()
        return 0


if __name__ == '__main__':
    sys.exit(main())
