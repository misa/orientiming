# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# ******************************************************************************

import io
from setuptools import setup


with io.open('README.md', "r", encoding="utf-8") as f:
    long_description = f.read()

setup(
    name='orientiming',
    version='0.3',
    description='Orientiming is a framework for accessing data from SportIdent equipment',  # noqa
    long_description=long_description,
    author='Mihai Ibanescu',
    author_email='mihai.ibanescu@gmail.com',
    url='http://bitbucket.org/misa/orientiming',
    license='Eclipse Public Lincense',
    packages=['orientiming'],
    classifiers=[
        "License :: OSI Approved :: Eclipse Public License 1.0 (EPL-1.0)",
        "Topic :: Utilities",
        "Operating System :: MacOS :: MacOS X",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 2.7",
        ],
    platforms=[
        "Linux",
        ],
    entry_points={
        "console_scripts": [
            "orientiming=orientiming.readout:main",
            "orientiming-reprocess-data=orientiming.processRawData:main",
            "orientiming-reprint-last=orientiming.reprint:main",
            "orientiming-server=orientiming.ui.web:main",
            ],
        },
    scripts=["commands/br-print.sh",
             "commands/create-paper-sizes.sh"],
    install_requires=[
        "attrs",
        "pyserial~=3.4",
        "pyserial-asyncio~=0.4",
        "aiohttp~=3.0",
        "pyramid",
        "reportlab~=3.4",
        "six~=1.11",
        "sqlalchemy~=1.2",
        ],
    setup_requires=["pytest-runner"],
)
