#!/usr/bin/python

# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

import collections
import six

try:
    from unittest import mock
except ImportError:
    import mock

import testbase

from orientiming import silib

call = mock.call


class StreamData():
    @classmethod
    def reset(cls):
        cls._data_written = bytearray()
        cls._data_read = collections.deque()

    class Stream(object):
        def read(self, size):
            ret = []
            ret_len = 0
            while 1:
                data = StreamData._data_read.popleft()
                if ret_len + len(data) > size:
                    useful = data[:size - ret_len]
                    rest = data[size - ret_len:]
                    StreamData._data_read.appendleft(rest)
                    data = useful
                ret.append(data)
                ret_len += len(data)
                if ret_len == size:
                    break
            return b''.join(ret)

        def write(self, data):
            StreamData._data_written.extend(bytearray(data))


class SerialReaderTest(testbase.BaseTest):
    def setUp(self):
        testbase.BaseTest.setUp(self)
        StreamData.reset()

    @mock.patch("orientiming.silib.serial.Serial")
    def test_connect(self, _Serial):
        r = silib.SerialReader("/dev/null")
        _Serial.return_value = StreamData.Stream()

        StreamData._data_read.extend([
            silib.c.SIPROTO_STX,
            silib.c.SIPROTO_STX,
            silib.c.SIPROTO_STX,
            silib.c.SIPROTO_STX,
            # New cmd
            silib.c.SICMD_SET_MS_MODE,
            # Param len
            six.int2byte(7),
            b'\0\0\0\0\0\0\0',
            # crc
            b'\x85\x71',
            # Proto terminator
            silib.c.SIPROTO_ETX,

            b'\x02\x83\x04\x00\x13\x74\x05\xb0\x0c\x03',
            b'\x02\x83\x06\x00\x13\x05\x36\x35\x36\xcb\x5e\x03',
            b'\x02\x83\x04\x00\x13\x33\xc1\x20\x92\x03',
        ])
        r.connect()

        expected = [
            b'\xff\x02\x02\xf0\x01\x4d\x6d\x0a\x03',
            b'\x02\x83\x02\x74\x01\x04\x14\x03',
            b'\x02\x83\x02\x05\x03\x22\x1e\x03',
            b'\x02\x83\x02\x33\x01\x16\x11\x03',
        ]
        self.assertEqual(
            b''.join(expected),
            bytes(StreamData._data_written))


class ReaderTest(testbase.BaseTest):
    def test_readOnePacket(self):
        stream = mock.MagicMock()
        stream.read.side_effect = [
            b'j', b'u', b'n', b'k',
            silib.c.SIPROTO_STX,
            silib.c.SIPROTO_STX,
            silib.c.SICMD_SET_MS_MODE,
            # Param len
            six.int2byte(7),
            b'\x00' * 7,
            # crc
            b'\x85\x71',
            # Proto terminator
            silib.c.SIPROTO_ETX,
        ]
        cmd, params, terminator = silib.readOnePacket(stream)
        self.assertEqual(
            [
                call(1), call(1), call(1), call(1),
                call(1), call(1), call(1), call(1),
                call(10), call(3), call(1),
            ],
            stream.read.call_args_list)

    def test_readOnePacket_Timeout(self):
        stream = mock.MagicMock()
        stream.read.side_effect = [
            b'']
        with self.assertRaises(silib.TimeoutError) as ctx:
            silib.readOnePacket(stream)
        self.assertEqual("", str(ctx.exception))

    def test_readOnePacket_UnknownCommand(self):
        stream = mock.MagicMock()
        stream.read.side_effect = [
            silib.c.SIPROTO_STX,
            b'\x42',
            ]
        with self.assertRaises(silib.UnknownCommand) as ctx:
            silib.readOnePacket(stream)
        self.assertEqual((b'\x42', ),
                         ctx.exception.args)

    def test_readOnePacket_UnknownCommand_0xC4(self):
        stream = mock.MagicMock()
        stream.read.side_effect = [
            silib.c.SIPROTO_STX,
            b'\xC4',
            ]
        with self.assertRaises(silib.UnknownCommand) as ctx:
            silib.readOnePacket(stream)
        self.assertEqual((b'\xC4', ),
                         ctx.exception.args)

    def test_readOnePacket_IncompleteRead(self):
        stream = mock.MagicMock()
        stream.read.side_effect = [
            silib.c.SIPROTO_STX,
            silib.c.SICMD_SET_MS_MODE,
            b'',
            ]
        with self.assertRaises(silib.IncompleteReadError) as ctx:
            silib.readOnePacket(stream)
        self.assertEqual("", str(ctx.exception))

    def test_readOnePacket_InvalidResponse(self):
        stream = mock.MagicMock()
        stream.read.side_effect = [
            silib.c.SIPROTO_STX,
            silib.c.SICMD_SET_MS_MODE,
            # Param len
            six.int2byte(7),
            b'\x00' * 3,
            b'',
            ]
        with self.assertRaises(silib.InvalidResponseError) as ctx:
            silib.readOnePacket(stream)
        self.assertEqual("", str(ctx.exception))

    def test_readOnePacket_InvalidCRC(self):
        stream = mock.MagicMock()
        stream.read.side_effect = [
            silib.c.SIPROTO_STX,
            silib.c.SICMD_SET_MS_MODE,
            # Param len
            six.int2byte(7),
            b'\x00' * 7,
            # crc - invalid
            b'\x00\x00',
            silib.c.SIPROTO_ETX,
            ]
        with self.assertRaises(silib.InvalidCRCError) as ctx:
            silib.readOnePacket(stream)
        self.assertEqual((b'\x85\x71', b'\x00\x00'),
                         ctx.exception.args)


class CommandTest(testbase.BaseTest):
    def testSendCommand(self):
        ret = silib.sendCommand(
            silib.c.SICMD_SET_MS_MODE,
            silib.c.SIPARAM_MS_MASTER, wakeUp=True)
        self.assertEqual(
            'ff0202f0014d6d0a03',
            ''.join("%02x" % x for x in bytearray(ret)))


if __name__ == '__main__':
    testbase.sys.exit(testbase.main())
