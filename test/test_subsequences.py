#!/usr/bin/python

# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

import testbase

from orientiming import subsequences


class SubsequencesTest(testbase.BaseTest):
    def testLongestCommonSubsequence(self):
        tests = [
            ([1], [2, 3, 4], []),
            (['30', '31', '32'], ['30', '31', '32'],
             ['30', '31', '32']),
            (['30', '31', '32', '33'], ['30', '31', '32'],
             ['30', '31', '32']),
            (['30', '31', '32'], ['30', '33', '31', '34', '32', '35'],
             ['30', '31', '32']),
            (['30', '33', '31', '34', '32', '35'], ['30', '31', '32'],
             ['30', '31', '32']),
            (['30', '32', '33', '31', '32'], ['30', '31', '32'],
             ['30', '31', '32']),
            (['30', '32', '33', '31'], ['30', '31', '32'],
             ['30', '32']),
            ([51, 52, 53, 54, 55, 56, 57, 58, 59, 49, 61, 62, 63],
             [51, 52, 53, 54, 55, 56, 57, 58, 59, 49, 61, 62, 63],
             [51, 52, 53, 54, 55, 56, 57, 58, 59, 49, 61, 62, 63]),
            ([51, 52, 53, 54, 55, 56, 57, 58, 59, 49, 61, 62, 63],
             [51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71, 73, 75, 77, 79, 81],
             [51, 53, 55, 57, 59, 61, 63]),
        ]
        for s1, s2, lcs in tests:
            self.assertEqual(
                subsequences.longestCommonSubsequence(s1, s2), lcs)
