#!/usr/bin/python

# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

import tempfile
import testbase

from orientiming import courses


class DatabaseTest(testbase.BaseTest):
    def _writeCourseFile(self, contents):
        t = tempfile.NamedTemporaryFile(dir=self.workDir, prefix="courses.",
                                        delete=False, mode="w+")
        t.write(contents)
        t.close()
        return t.name

    def testParseCourse(self):
        contents = """
A = 41,42,43,44 = 1.0 K
B = 51, 52,53,54 = 1.0 K
"""
        courseFile = self._writeCourseFile(contents)
        c = courses.Courses(courseFile)
        self.assertEqual(
            [x.name for x in c.courses],
            ['A', 'B'])
        self.assertEqual(
            [x.controls for x in c.courses],
            [
                [41, 42, 43, 44],
                [51, 52, 53, 54],
            ])

    def testMatchCourse(self):
        contents = """
A = 41,42,43,44 = 1.0 K
B = 51, 53,54 = 1.0 K
C = 51, 52,53,54 = 1.0 K
"""
        courseFile = self._writeCourseFile(contents)
        c = courses.Courses(courseFile)
        self.assertEqual(c.match([41, 42, 43, 44]), 'A')
        self.assertEqual(c.match([51, 52, 53, 54]), 'C')

    def testMatchCourseSubset(self):
        contents = """
A = 41,42,43,44,45 = 1.0 K
B = 41,42,43 = 1.0 K
"""
        courseFile = self._writeCourseFile(contents)
        c = courses.Courses(courseFile)
        m = c.matchPartial([41, 42, 43, 44])
        self.assertEqual(m.matched, True)
        self.assertEqual(m.course.name, 'B')
        m = c.matchPartial([41, 42, 43])
        self.assertEqual(m.matched, True)
        self.assertEqual(m.course.name, 'B')

    def testMatchCourseSubset2(self):
        contents = """
Brown=79,78,77,76,75,74,73,72,71,70,69,68,67,66,65,64,63,62,61,60=4.0km
Red=40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64=7.1km
Green=40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55=5.1km
Bow=40,41,42,43,44,42,45,46,40=2.5km
LeftBow=40,41,42,43=1.5km
RightBow=40,42,45,46,40=1.7km
"""  # noqa
        courseFile = self._writeCourseFile(contents)
        c = courses.Courses(courseFile)
        m = c.matchPartial(
            [40, 41, 60, 42, 43, 44, 62, 45, 43, 44, 46, 47, 48, 49,
             68, 50, 51, 52, 71, 70, 53, 54, 55])
        self.assertEqual(m.matched, True)
        self.assertEqual(m.course.name, 'Green')

    def testMatchPartial(self):
        contents = """
B = 41,42=2.0
A = 40,41,42,43,44 = 1.0 K
"""
        courseFile = self._writeCourseFile(contents)
        c = courses.Courses(courseFile)
        m = c.matchPartial([41, 42, 45, 43, 46])
        self.assertEqual(m.matched, True)
        self.assertEqual(m.course.name, 'B')
        self.assertEqual(
            m.diff,
            [
                ('*', 41),
                ('*', 42),
                ('+', 45),
                ('+', 43),
                ('+', 46),
            ])

    def testMatchPartialTailMissing(self):
        contents = """
A = 40,41,42,43,44 = 1.0 K
"""
        courseFile = self._writeCourseFile(contents)
        c = courses.Courses(courseFile)
        m = c.matchPartial([41, 42])
        self.assertEqual(m.matched, False)
        self.assertEqual(m.course.name, 'A')
        self.assertEqual(
            m.diff,
            [
                ('-', 40),
                ('*', 41),
                ('*', 42),
                ('-', 43),
                ('-', 44),
            ])

    def testMatchPartialEmpty(self):
        contents = ""
        courseFile = self._writeCourseFile(contents)
        c = courses.Courses(courseFile)
        m = c.matchPartial([41, 42])
        self.assertEqual(None, m.matched)
        self.assertEqual(None, m.course)
        self.assertEqual([], m.diff)
