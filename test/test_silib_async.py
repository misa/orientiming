#!/usr/bin/python

# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

import six

try:
    from unittest import mock
except ImportError:
    import mock

import asyncio
import testbase

from orientiming import silib_async, siconst as c

call = mock.call


def async_test(f):
    def wrapper(slf, *args, **kwargs):
        loop = slf.loop
        coro = asyncio.coroutine(f)
        future = coro(slf, *args, **kwargs)
        loop.run_until_complete(future)
    return wrapper


class AIOTest(testbase.BaseTest):
    def setUp(self):
        super(AIOTest, self).setUp()
        self.loop = asyncio.new_event_loop()


@mock.patch("orientiming.silib_async.serial_asyncio.open_serial_connection")
class SerialReaderTest(AIOTest):

    @mock.patch("orientiming.silib_async.SerialReader.run_transport")
    @mock.patch("orientiming.silib_async.SerialReader.post_connect")
    @mock.patch("orientiming.silib_async.SerialReader.connect_at_speed")
    @async_test
    async def test_connect(self, _connect_at_speed, _post_connect,
                           _run_transport,
                           _open_serial_connection):
        tty = "/dev/idle"
        ser = silib_async.SerialReader(self.loop, tty)
        reader = mock.MagicMock()
        writer = mock.MagicMock()
        osc = mock.MagicMock(return_value=(reader, writer))
        _connect_at_speed.side_effect = asyncio.coroutine(osc)
        _run_transport.side_effect = asyncio.coroutine(mock.MagicMock())
        _post_connect.side_effect = asyncio.coroutine(mock.MagicMock())

        await ser.connect()

        _connect_at_speed.assert_called_once_with(38400)
        _post_connect.assert_called_once_with(38400)
        _run_transport.assert_called_once_with()

    @mock.patch("orientiming.silib_async.SerialReader.run_transport")
    @mock.patch("orientiming.silib_async.SerialReader.post_connect")
    @mock.patch("orientiming.silib_async.SerialReader.connect_at_speed")
    @async_test
    async def test_connect__retry(self, _connect_at_speed, _post_connect,
                                  _run_transport,
                                  _open_serial_connection):
        tty = "/dev/idle"
        ser = silib_async.SerialReader(self.loop, tty)
        reader = mock.MagicMock()
        writer = mock.MagicMock()
        osc = mock.MagicMock(side_effect=[
            silib_async.silib.RetryNextSpeed,
            silib_async.silib.RetryNextSpeed,
            (reader, writer)])
        _connect_at_speed.side_effect = asyncio.coroutine(osc)
        _run_transport.side_effect = asyncio.coroutine(mock.MagicMock())
        _post_connect.side_effect = asyncio.coroutine(mock.MagicMock())

        await ser.connect()

        self.assertEqual(
            [call(38400), call(4800), call(38400)],
            _connect_at_speed.call_args_list)
        _post_connect.assert_called_once_with(38400)
        _run_transport.assert_called_once_with()

    @async_test
    async def test_connect_at_speed(self, _open_serial_connection):
        tty = "/dev/idle"
        baud = 12345
        reader = mock.MagicMock()
        writer = mock.MagicMock()
        osc = mock.MagicMock(return_value=(reader, writer))
        _open_serial_connection.side_effect = asyncio.coroutine(osc)

        reader_read = mock.MagicMock()
        reader_read.side_effect = [
            c.SIPROTO_STX,
            c.SIPROTO_STX,
            c.SIPROTO_STX,
            c.SIPROTO_STX,
            # New cmd
            c.SICMD_SET_MS_MODE,
            # Param len
            six.int2byte(7),
            b'\0\0\0\0\0\0\0',
            # crc
            b'\x85\x71',
            # Proto terminator
            c.SIPROTO_ETX,
        ]
        reader.read.side_effect = asyncio.coroutine(reader_read)

        ser = silib_async.SerialReader(self.loop, tty)
        ret = await ser.connect_at_speed(baud)
        self.assertEqual((reader, writer), ret)

        osc.assert_called_once_with(baudrate=baud, url=tty)
        writer.write.assert_called_once_with(b"\xff\x02\x02\xf0\x01\x4d\x6d\x0a\x03")

    @mock.patch("orientiming.silib_async.log.info")
    @async_test
    async def test_post_connect(self, _log_info, _open_serial_connection):
        tty = "/dev/idle"
        baud = 38400
        reader = mock.MagicMock()
        writer = mock.MagicMock()
        osc = mock.MagicMock(return_value=(reader, writer))
        _open_serial_connection.side_effect = asyncio.coroutine(osc)

        reader_read = mock.MagicMock()
        reader_read.side_effect = [
            c.SIPROTO_STX, b"\x83", b"\x07", b"\x00\x13\x71\x05\x13\x35\x05\x88\xac\x03",
            c.SIPROTO_STX, b"\x83", b"\x06", b"\x00\x13\x05\x36\x35\x36\xcb\x5e\x03",
        ]
        reader.read.side_effect = asyncio.coroutine(reader_read)

        ser = silib_async.SerialReader(self.loop, tty)
        ser._reader = reader
        ser._writer = writer

        ret = await ser.post_connect(baud)
        self.assertEqual(None, ret)

        self.assertEqual(
            [
                call(b"\x02\x83\x02\x71\x04\x1a\x0a\x03"),
                call(b"\x02\x83\x02\x05\x03\x22\x1e\x03"),
            ],
            writer.write.call_args_list)
        self.assertEqual(
            [
                call('Configuration:'),
                call('  Station Code: %d', 19),
                call('  Software Version: %s', '6.56'),
                call('  Station mode: %s', 'READOUT'),
                call('  Extended Protocol: %s', True),
                call('  Auto Send Out: %s', False),
                call('  Handshake: %s', True),
                call('  Access With Password: %s', False),
                call('  Readout After Punch: %s', False),
            ],
            _log_info.call_args_list)

    @async_test
    async def test_post_connect__reset_speed(self, _open_serial_connection):
        tty = "/dev/idle"
        baud = 12345
        reader = mock.MagicMock()
        writer = mock.MagicMock()
        osc = mock.MagicMock(return_value=(reader, writer))
        _open_serial_connection.side_effect = asyncio.coroutine(osc)

        reader_read = mock.MagicMock()
        reader_read.side_effect = [
            c.SIPROTO_STX, b"\xfe", b"\x03", b"\00\00\x01\x99\x17\x03",
        ]
        reader.read.side_effect = asyncio.coroutine(reader_read)

        ser = silib_async.SerialReader(self.loop, tty)
        ser._reader = reader
        ser._writer = writer

        with self.assertRaises(silib_async.silib.RetryNextSpeed) as ctx:
            await ser.post_connect(baud)
        self.assertEqual((), ctx.exception.args)

        self.assertEqual(
            [
                call(b"\x02\xfe\x01\x01\x85\x09\x03"),
            ],
            writer.write.call_args_list)

    @mock.patch("orientiming.silib_async.log.debug")
    def test_run_transport(self, _log_debug, _open_serial_connection):
        tty = "/dev/idle"
        ser = silib_async.SerialReader(self.loop, tty)
        ser._writer = mock.MagicMock()
        self._run_transport(ser)
        _log_debug.assert_called_once_with("Connection made")
        ser._writer.transport._ensure_reader.assert_called_once_with()

    @async_test
    async def _run_transport(self, ser):
        return await ser.run_transport()
