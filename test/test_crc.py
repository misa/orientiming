#!/usr/bin/python

# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

import testbase
from orientiming import crc529


class CRCTest(testbase.BaseTest):
    def testCrc(self):
        tests = [
            ('', b'\x00\x00'),
            ('a', b'\x00\x00'),
            (b'ab', b'\x61\x62'),
            (b'abc', b'\xa4\x49'),
            (b'abcd', b'\x58\xe7'),
            (b'abcde', b'\x3d\xe7'),
            (b'abcdef', b'\x0d\x05'),
            (b'abcdefg', b'\x6a\x05'),
            (b'abcdefgh', b'\x7d\x68'),
            (b'\0\0\0', b'\x00\x00'),
            (b'\0\0\0\0', b'\x00\x00'),
            (b'\0\0\0\0\0', b'\x00\x00'),
            (b'\xFF\xFF\xFF', b'\x7f\x0d'),
            (b'\xFF\xFF\xFF\xFF', b'\x80\x29'),
        ]
        for s, expVal in tests:
            self.assertEqual(crc529.crc529(s), expVal)
