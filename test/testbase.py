#!/usr/bin/python

# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

import os
import shutil
import sys
import unittest

main = unittest.main


class BaseTest(unittest.TestCase):
    codePath = os.path.abspath(os.path.normpath(
        os.path.join(os.path.dirname(__file__), '..')))

    if codePath not in sys.path:
        sys.path.insert(0, codePath)

    archiveDir = os.path.abspath(os.path.normpath(
        os.path.join(os.path.dirname(__file__), 'test-data')))

    @classmethod
    def getTestFilePath(cls, fileName):
        fp = os.path.join(cls.archiveDir, fileName)
        if not os.path.exists(fp):
            raise RuntimeError("Unable to find file %s" % fp)
        return fp

    def setUp(self):
        super(BaseTest, self).setUp()
        self.workDir = "/tmp/workDir"
        shutil.rmtree(self.workDir, ignore_errors=True)
        os.makedirs(self.workDir)
