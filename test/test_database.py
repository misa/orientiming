#!/usr/bin/python

# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

import os

import testbase

from orientiming import database


class DatabaseTest(testbase.BaseTest):
    def setUp(self):
        testbase.BaseTest.setUp(self)

        self.dbpath = os.path.join(self.workDir, "db.sqlite")
        self.db = database.Database(self.dbpath)

    def _getCourses(self):
        sess = self.db.sess
        results = sess.execute("SELECT course_id, name FROM Courses")
        results = sorted(results, key=lambda x: x.course_id)
        return results

    def testAddCourses(self):
        self.db.addCourses(
            ["White", "Yellow", "Orange", "Brown", "Green", "Red"])
        self.db.commit()
        results = self._getCourses()
        self.assertEqual(
            sorted(x.name for x in results),
            ['Brown', 'Green', 'Orange', 'Red', 'White', 'Yellow'])

    def testAddResults(self):
        ret = self.db.getResultRank(12345, "John", 150, "White")
        rank, totalRuns, diffFromLead = ret
        self.assertEqual(ret, (1, 1, 0))

        ret = self.db.getResultRank(12346, "Mary", 160, "White")
        self.assertEqual(ret, (2, 2, 10))
        ret = self.db.getResultRank(12343, "Bob", 130, "White")
        self.assertEqual(ret, (1, 3, 0))
        ret = self.db.getResultRank(12340, "Violet", 100, "White")
        self.assertEqual(ret, (1, 4, 0))
        ret = self.db.getResultRank(12344, "Scarlet", 140, "White")
        self.assertEqual(ret, (3, 5, 40))

        ret = self.db.getResultRank(22344, "Tim", 240, "Yellow")
        self.assertEqual(ret, (1, 1, 0))

        ret = self.db.getResultRank(12347, "Scott", 170, "White")
        self.assertEqual(ret, (6, 6, 70))

        # Double-punch, should not change anything
        ret = self.db.getResultRank(12346, "Mary", 160, "White")
        self.assertEqual(ret, (5, 6, 60))

        # Triple-punch
        ret = self.db.getResultRank(12346, "Mary", 160, "White")
        self.assertEqual(ret, (5, 6, 60))

        self.assertEqual(
            [x.name for x in self._getCourses()],
            ['White', 'Yellow'])

    def testAddResultsWithDNF(self):
        self.testAddResults()
        # DNF
        ret_dnf = self.db.getResultRank(12321, "Ignored", 90, "White",
                                        is_dnf=True)
        self.assertEqual((None, None, None), ret_dnf)

        # Triple-punch, but total runs should go up
        ret = self.db.getResultRank(12346, "Mary", 160, "White")
        self.assertEqual(ret, (5, 7, 60))

    def testAddResultsWithClasses(self):
        self.db.commit()
        Course = database.Models.Course
        whiteCourse = Course('White')
        yellowCourse = Course('Yellow')
        K = database.Models.Class
        classes = [
                K('W21', whiteCourse),
                K('W31', whiteCourse),
                K('Y21', yellowCourse),
                K('Y31', yellowCourse),
                ]
        self.db.addClasses(classes)
        self.db.commit()

        w21 = self.db.getClass('W21')
        y21 = self.db.getClass('Y21')

        ev = self.db.event
        E = database.Models.EventEntry
        entries = [
                E(12345, 'John J', ev, w21),
                E(12346, 'Mary M', ev, w21),
                E(12343, 'Bobby B', ev, w21),
                E(12340, 'Violet V', ev, w21),
                E(12344, 'Scarlet S', ev, w21),
                E(22344, 'Tim T', ev, y21),
                E(12347, 'Scott S', ev, w21),
                ]

        self.db.addEntries(entries)
        self.db.commit()
        self.testAddResults()

        R = database.Models.Result
        EE = database.Models.EventEntry
        ret = self.db.sess.query(R).join(EE).join(K).filter(
                K.name == 'W21').count()
        self.assertEqual(ret, 6)
        ret = self.db.sess.query(R).join(EE).join(K).filter(
                K.name == 'Y21').count()
        self.assertEqual(ret, 1)
        # No unknown competitors
        ret = self.db.sess.query(R).join(EE).join(Course).count()
        self.assertEqual(ret, 0)

        # Add unknown competitors
        ret = self.db.getResultRank(32340, "Wile E. Coyote", 360, "Yellow")
        self.assertEqual(ret, (1, 1, 0))
        ret = self.db.getResultRank(32341, "Roadrunner", 350, "Yellow")
        self.assertEqual(ret, (1, 2, 0))

        ret = self.db.sess.query(R).join(EE).join(Course).count()
        self.assertEqual(ret, 2)

        ret = self.db.sess.query(R).join(EE).join(Course).filter(
                Course.name == 'Yellow').count()
        self.assertEqual(ret, 2)
