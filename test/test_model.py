#!/usr/bin/python

# ******************************************************************************
# Copyright (c) Mihai Ibanescu
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html
#
# Contributors:
#    Mihai Ibanescu <mihai.ibanescu@gmail.com> - initial API and implementation
# ******************************************************************************

from __future__ import print_function
from __future__ import unicode_literals

import os
import time
from io import StringIO

import testbase

from orientiming import courses
from orientiming import datahandler
from orientiming import model
from orientiming import config
from orientiming import printout


class ModelTest(testbase.BaseTest):
    CardToCsvData = [
        ('card5-2', b'221779,21779,,,,,,,,,,,,,,,,,,,,,00:24:43,,,00:44:48,13,51,,00:26:13,52,,00:27:44,53,,00:28:36,54,,00:30:03,55,,00:31:56,56,,00:33:30,57,,00:35:41,58,,00:38:18,59,,00:39:02,49,,00:40:19,61,,00:42:12,62,,00:43:11,63,,00:44:11'),  # noqa
        ('card6-1', b'670715,1,,Artem,Kazantsev,BOK,RU,,,,,,,,1,Su,13:40:33,,,,2,Su,13:41:32,7,Su,14:02:03,12,64,Su,13:42:03,65,Su,13:43:15,67,Su,13:44:56,69,Su,13:47:06,70,Su,13:48:37,71,Su,13:50:47,72,Su,13:52:23,73,Su,13:54:10,74,Su,13:57:12,75,Su,13:58:27,76,Su,13:59:51,77,Su,14:01:38'),  # noqa
        ('card6-2', b'670715,1,,Artem,Kazantsev,BOK,RU,,,,,,,,1,Su,14:06:25,,,,2,Su,14:07:19,7,Su,14:26:57,13,51,Su,14:08:27,52,Su,14:09:55,53,Su,14:10:42,54,Su,14:12:28,55,Su,14:14:39,56,Su,14:16:23,57,Su,14:18:09,58,Su,14:19:59,59,Su,14:20:44,49,Su,14:22:08,61,Su,14:24:02,62,Su,14:25:04,63,Su,14:26:26'),  # noqa
        ('card-data-1', b'2005141,,,Mihai,Ibanescu,,,,,,,,,,1,Su,14:31:28,1,Su,14:31:28,2,Su,14:31:29,7,Su,14:58:05,13,51,Su,14:32:36,52,Su,14:34:19,53,Su,14:35:14,54,Su,14:36:41,55,Su,14:40:10,56,Su,14:42:58,57,Su,14:47:33,58,Su,14:49:29,59,Su,14:50:24,49,Su,14:52:05,61,Su,14:54:22,62,Su,14:55:35,63,Su,14:57:19'),  # noqa
        ('pCard-1', b'4710067,,,Holly,Kuestner,,US,m,,,,,,,1,Su,13:30:47,1,Su,13:30:47,1,Su,13:27:21,2,Su,14:35:17,9,61,Su,13:38:36,62,Su,13:41:11,63,Su,13:49:22,64,Su,13:56:43,68,Su,14:05:53,69,Su,14:12:01,70,Su,14:18:25,71,Su,14:28:35,72,Su,14:34:16'),  # noqa
    ]

    def testPacketLengths(self):
        self.assertEqual(model.SI_Card_5.length, 128)
        self.assertEqual(model.SI_Card_6.length, 1024)
        self.assertEqual(model.SI_Card_8.length, 256)
        self.assertEqual(model.SI_Card_9.length, 256)
        self.assertEqual(model.SI_pCard.length, 256)
        self.assertEqual(model.SI_tCard.length, 256)

    def testBasic(self):
        tests = [
            ('card5-1', 239300, 39300, b'', b'', b'', b'', 11),
            ('card5-2', 221779, 21779, b'', b'', b'', b'', 13),
            ('card6-1', 670715, 1, b'Artem', b'Kazantsev', b'RU', b'BOK', 12),
            ('card-data-1', 2005141, b'', b'Mihai', b'Ibanescu', b'', b'', 13),
            ('card-data-8', 2005099, b'', b'*** BOK', b'RENTAL ***', b'', b'', 0),
        ]
        for tup in tests:
            (cardFile, cardId, startNumber, name1, name2, country, club,
             recordCount) = tup
            data = open(self.getTestFilePath(cardFile), "rb").read()
            cd = model.SI_CardData(data)
            self.assertEqual(cd.card_id, cardId)
            self.assertEqual(cd.start_number, startNumber)
            self.assertEqual(cd.name_1, name1)
            self.assertEqual(cd.name_2, name2)
            self.assertEqual(cd.country, country)
            self.assertEqual(cd.club, club)
            self.assertEqual(cd.record_count, recordCount)

    def testTimes(self):
        tests = [
            ('card5-1', b'', b'', b'', b'11:37:47', b'', b'12:04:34'),
            ('card5-2', b'', b'', b'', b'00:24:43', b'', b'00:44:48'),
            ('card6-1', b'Su', b'13:40:33', b'Su', b'13:41:32', b'Su',
             b'14:02:03'),
            ('card-data-1', b'Su', b'14:31:28', b'Su', b'14:31:29', b'Su',
             b'14:58:05'),
            ('card-data-8', b'We', b'21:23:00', b'', b'', b'', b''),
        ]
        for tup in tests:
            (cardFile, dayClear, timeClear, dayStart, timeStart, dayFinish,
             timeFinish) = tup
            data = open(self.getTestFilePath(cardFile), "rb").read()
            cd = model.SI_CardData(data)
            self.assertEqual(cd.time_clear.getDayOfWeek(), dayClear)
            self.assertEqual(cd.time_clear.asTimeString(), timeClear)
            self.assertEqual(cd.time_start.getDayOfWeek(), dayStart)
            self.assertEqual(cd.time_start.asTimeString(), timeStart)
            self.assertEqual(cd.time_finish.getDayOfWeek(), dayFinish)
            self.assertEqual(cd.time_finish.asTimeString(), timeFinish)

    def testPunches(self):
        tests = [
            ('card5-1', [
                '62,,11:39:13', '63,,11:42:48', '64,,11:46:51',
                '65,,11:48:05', '66,,11:50:12', '67,,11:51:41',
                '68,,11:54:42', '69,,11:57:23', '70,,11:58:49',
                '40,,12:01:07', '41,,12:04:06',
                ]),
            ('card5-2', [
                '51,,00:26:13', '52,,00:27:44', '53,,00:28:36', '54,,00:30:03',
                '55,,00:31:56', '56,,00:33:30', '57,,00:35:41', '58,,00:38:18',
                '59,,00:39:02', '49,,00:40:19', '61,,00:42:12', '62,,00:43:11',
                '63,,00:44:11',
                ]),
            ('card6-1', [
                '64,Su,13:42:03', '65,Su,13:43:15', '67,Su,13:44:56',
                '69,Su,13:47:06', '70,Su,13:48:37', '71,Su,13:50:47',
                '72,Su,13:52:23', '73,Su,13:54:10', '74,Su,13:57:12',
                '75,Su,13:58:27', '76,Su,13:59:51', '77,Su,14:01:38',
                ]),
            ('card-data-1', [
                '51,Su,14:32:36', '52,Su,14:34:19', '53,Su,14:35:14',
                '54,Su,14:36:41', '55,Su,14:40:10', '56,Su,14:42:58',
                '57,Su,14:47:33', '58,Su,14:49:29', '59,Su,14:50:24',
                '49,Su,14:52:05', '61,Su,14:54:22', '62,Su,14:55:35',
                '63,Su,14:57:19',
                ]),
            ('card-data-8', [
                ]),
        ]
        for cardFile, punches in tests:
            data = open(self.getTestFilePath(cardFile), "rb").read()
            cd = model.SI_CardData(data)
            self.assertEqual([x.toCsv() for x in cd.punches],
                             [x.encode('ascii') for x in punches])

    def testRecordToCsv(self):
        fmt = model.SI_CardData.formatDateToString
        now = 1234567890.123
        for cardFile, expCsv in self.CardToCsvData:
            data = open(self.getTestFilePath(cardFile), "rb").read()
            cd = model.SI_CardData(data)
            expCsv = b"%d,%s,%s" % (1, fmt(now), expCsv)
            self.assertEqual(cd.toCsv(downloadTime=now), expCsv)
            now += 1

    def test_SI_Card_to_array(self):
        fmt = model.SI_CardData.formatDateToString
        now = 1234567890.123
        for cardFile, expCsv in self.CardToCsvData:
            data = open(self.getTestFilePath(cardFile), "rb").read()
            cddata = model.SI_CardData(data)
            csv_str = cddata.toCsv(downloadTime=now, idx=1)

            cd = model.SI_Card.from_csv(csv_str)
            arr = csv_str.split(b',')
            arr[1] = fmt(now)

            self.assertEqual(
                arr,
                [str(x).encode('ascii') for x in cd.to_array()])


class ConfigBase(testbase.BaseTest):
    def setUp(self):
        super(ConfigBase, self).setUp()
        self.cfg = config.Configuration()
        self.cfg.read()
        self.cfg.dataPath = self.workDir
        self.cfg.cp.set(self.cfg.DefaultSection, 'csvFilePrefix',
                        'Jean')
        self.cfg.cp.set(self.cfg.DefaultSection, 'eventTitle',
                        'Birkshire Championship Classics')
        self.cfg.cp.set(self.cfg.DefaultSection, 'eventDate',
                        'March 9-11, 2012')
        open(self.cfg.namesMapFile, "w").write("""
# This is a comment
221779,Josef,Trzicky
""")

        # Create dummy courses file
        coursesL = [
            'Red=71,74,66,47,51,60,58,61,62,55,78,108,104,110=7.3K',
            'Green=70,63,68,44,45,46,59,57,53,77,103,110= 5.8K',
        ]
        f = open(self.cfg.coursesFilePath, "w")
        for course in coursesL:
            f.write(course)
            f.write('\n')
        f.close()


class ConfigBackedTest(ConfigBase):
    def testRawWriter(self):
        rw = datahandler.DataHandler(self.cfg)

        expected = []

        fname = 'card-data-1'
        data = open(self.getTestFilePath(fname), "rb").read()
        blocks = self._splitBlocks(data, [0, 1])
        rw.write(blocks)
        expected.append([x[1] for x in ModelTest.CardToCsvData
                         if x[0] == fname][0])
        time.sleep(.01)

        fname = 'card5-2'
        data = open(self.getTestFilePath(fname), "rb").read()
        blocks = self._splitBlocks(data, [0])
        rw.write(blocks)
        expected.append([x[1] for x in ModelTest.CardToCsvData
                         if x[0] == fname][0])
        time.sleep(.01)

        fname = 'card6-1'
        data = open(self.getTestFilePath(fname), "rb").read()
        blocks = self._splitBlocks(data, [0, 6, 7])
        rw.write(blocks)
        expected.append([x[1] for x in ModelTest.CardToCsvData
                         if x[0] == fname][0])
        time.sleep(.01)

        fname = 'pCard-1'
        data = open(self.getTestFilePath(fname), "rb").read()
        blocks = self._splitBlocks(data, [0, 1])
        rw.write(blocks)
        expected.append([x[1] for x in ModelTest.CardToCsvData
                         if x[0] == fname][0])

        flist = sorted(os.listdir(self.cfg.csvDataPath))
        self.assertEqual(len(flist), 4)

        timestrings = [
            model.SI_CardData.formatDateToString(float(f.split('-', 1)[1]))
            for f in flist]
        expStrings = [datahandler.CsvDirectoryReader.CsvHeader.encode('ascii')]
        expStrings.extend(b"%d,%s,%s" % (1, tstring, csvData)
                          for tstring, csvData in zip(timestrings, expected))

        reader = datahandler.CsvDirectoryReader(self.cfg.csvDataPath)
        dataStrings = [x.rstrip() for x in reader.toStream().readlines()]
        self.assertEqual(dataStrings, expStrings)

    @classmethod
    def _splitBlocks(cls, data, blocksList):
        ret = []
        for blockNo in blocksList:
            ret.append((blockNo, data[128 * blockNo:128 * (blockNo + 1)]))
        return ret


class PrintoutTest(ConfigBase):
    def testCardToTable(self):
        p = printout.Printout(self.cfg)

        fname = 'card-data-1'
        data = open(self.getTestFilePath(fname), "rb").read()
        cd = model.SI_CardData(data)

        table = p.cardToTable(cd)
        self.assertEqual(table, [
            ['Start', '', '14:31:29', 'Split', 'Time'],
            ['1.', 51, '14:32:36', '1:07', '1:07'],
            ['2.', 52, '14:34:19', '1:43', '2:50'],
            ['3.', 53, '14:35:14', '0:55', '3:45'],
            ['4.', 54, '14:36:41', '1:27', '5:12'],
            ['5.', 55, '14:40:10', '3:29', '8:41'],
            ['6.', 56, '14:42:58', '2:48', '11:29'],
            ['7.', 57, '14:47:33', '4:35', '16:04'],
            ['8.', 58, '14:49:29', '1:56', '18:00'],
            ['9.', 59, '14:50:24', '0:55', '18:55'],
            ['10.', 49, '14:52:05', '1:41', '20:36'],
            ['11.', 61, '14:54:22', '2:17', '22:53'],
            ['12.', 62, '14:55:35', '1:13', '24:06'],
            ['13.', 63, '14:57:19', '1:44', '25:50'],
            ['Finish', '', '14:58:05', '0:46', '26:36'],
        ])

        # Verify against a course
        sio = StringIO("""
Yellow=54,56,58,49,61=1
""")
        cs = courses.Courses(sio)

        fname = 'card5-2'
        data = open(self.getTestFilePath(fname), "rb").read()
        cd = model.SI_CardData(data)
        cdiff = cs.matchPartial(cd)
        table = p.cardToTable(cd, cdiff)
        self.assertEqual(table, [
            ['Start', '', '00:24:43', 'Split', 'Time'],
            ['++', 51, '00:26:13', '1:30', '1:30'],
            ['++', 52, '00:27:44', '1:31', '3:01'],
            ['++', 53, '00:28:36', '0:52', '3:53'],
            ['1.', 54, '00:30:03', '1:27', '5:20'],
            ['++', 55, '00:31:56', '1:53', '7:13'],
            ['2.', 56, '00:33:30', '1:34', '8:47'],
            ['++', 57, '00:35:41', '2:11', '10:58'],
            ['3.', 58, '00:38:18', '2:37', '13:35'],
            ['++', 59, '00:39:02', '0:44', '14:19'],
            ['4.', 49, '00:40:19', '1:17', '15:36'],
            ['5.', 61, '00:42:12', '1:53', '17:29'],
            ['++', 62, '00:43:11', '0:59', '18:28'],
            ['++', 63, '00:44:11', '1:00', '19:28'],
            ['Finish', '', '00:44:48', '0:37', '20:05'],
        ])

        # With a DNF-ed course
        sio = StringIO("""
Yellow=52,53,54,56,57,58,48,49,61=1
""")
        cs = courses.Courses(sio)
        cdiff = cs.matchPartial(cd)
        table = p.cardToTable(cd, cdiff)
        self.assertEqual(table, [
            ['Start', '', '00:24:43', 'Split', 'Time'],
            ['++', 51, '00:26:13', '1:30', '1:30'],
            ['1.', 52, '00:27:44', '1:31', '3:01'],
            ['2.', 53, '00:28:36', '0:52', '3:53'],
            ['3.', 54, '00:30:03', '1:27', '5:20'],
            ['++', 55, '00:31:56', '1:53', '7:13'],
            ['4.', 56, '00:33:30', '1:34', '8:47'],
            ['5.', 57, '00:35:41', '2:11', '10:58'],
            ['6.', 58, '00:38:18', '2:37', '13:35'],
            ['---', 48, '', '', ''],
            ['++', 59, '00:39:02', '0:44', '14:19'],
            ['8.', 49, '00:40:19', '1:17', '15:36'],
            ['9.', 61, '00:42:12', '1:53', '17:29'],
            ['++', 62, '00:43:11', '0:59', '18:28'],
            ['++', 63, '00:44:11', '1:00', '19:28'],
            ['Finish', '', '00:44:48', '0:37', '20:05'],
        ])

        # No matching course
        sio = StringIO("""
Yellow=81,82,83,84,85=1
""")

        cs = courses.Courses(sio)
        cdiff = cs.matchPartial(cd)
        table = p.cardToTable(cd, cdiff)
        self.assertEqual(table, [
            ['Start', '', '00:24:43', 'Split', 'Time'],
            ['---', 81, '', '', ''],
            ['---', 82, '', '', ''],
            ['---', 83, '', '', ''],
            ['---', 84, '', '', ''],
            ['---', 85, '', '', ''],
            ['++', 51, '00:26:13', '1:30', '1:30'],
            ['++', 52, '00:27:44', '1:31', '3:01'],
            ['++', 53, '00:28:36', '0:52', '3:53'],
            ['++', 54, '00:30:03', '1:27', '5:20'],
            ['++', 55, '00:31:56', '1:53', '7:13'],
            ['++', 56, '00:33:30', '1:34', '8:47'],
            ['++', 57, '00:35:41', '2:11', '10:58'],
            ['++', 58, '00:38:18', '2:37', '13:35'],
            ['++', 59, '00:39:02', '0:44', '14:19'],
            ['++', 49, '00:40:19', '1:17', '15:36'],
            ['++', 61, '00:42:12', '1:53', '17:29'],
            ['++', 62, '00:43:11', '0:59', '18:28'],
            ['++', 63, '00:44:11', '1:00', '19:28'],
            ['Finish', '', '00:44:48', '0:37', '20:05'],
        ])

    def testCardToTable_blank(self):
        p = printout.Printout(self.cfg)

        fname = 'card5-2'
        data = open(self.getTestFilePath(fname), "rb").read()
        cd = model.SI_CardData(data)
        cd.time_start.pth = 0xEE
        cd.time_finish.pth = 0xEE
        del cd.punches[:]

        sio = StringIO("""
Yellow=52,53,54,56,57,58,48,49,61=1
""")
        cs = courses.Courses(sio)
        cdiff = cs.matchPartial(cd)

        table = p.cardToTable(cd, cdiff)
        self.assertEqual(table, [
            ['Start', '', 'NO-START', 'Split', 'Time'],
            ['Finish', '', 'NO-FIN', 'XX:XX', 'XX:XX'],
        ])

    def testCardToTable_no_controls(self):
        p = printout.Printout(self.cfg)

        fname = 'card5-2'
        data = open(self.getTestFilePath(fname), "rb").read()
        cd = model.SI_CardData(data)
        del cd.punches[:]

        sio = StringIO("""
Yellow=51,52,53,54,55,56,57,58=1
""")
        cs = courses.Courses(sio)
        cdiff = cs.matchPartial(cd)

        table = p.cardToTable(cd, cdiff)
        self.assertEqual(table, [
            ['Start', '', '00:24:43', 'Split', 'Time'],
            ['Finish', '', '00:44:48', 'XX:XX', '20:05'],
        ])

    def testGeneratePdf(self):
        p = printout.Printout(self.cfg, courses.Names(self.cfg.namesMapFile))

        # With a DNF-ed course
        sio = StringIO("""
Yellow=52,53,54,56,57,58,48,49,61=1
""")
        cs = courses.Courses(sio)

        fname = 'card-data-1'
        data = open(self.getTestFilePath(fname), "rb").read()
        cd = model.SI_CardData(data)
        cdiff = cs.matchPartial(cd)
        p.generatePdf(cd, fname, cdiff)

        fname = 'card5-2'
        data = open(self.getTestFilePath(fname), "rb").read()
        cd = model.SI_CardData(data)
        cdiff = cs.matchPartial(cd)
        p.generatePdf(cd, fname, cdiff)

        # Longer course to force a mismatch at the very end
        sio = StringIO("""
Yellow=51,52,53,54,55,56,57,58,59,49,61,62,63,64=1
""")
        cs = courses.Courses(sio)
        cdiff = cs.matchPartial(cd)
        p.generatePdf(cd, fname, cdiff)

    def testGenerateText(self):
        sio = StringIO()
        p = printout.Printout(self.cfg)

        fname = 'card-data-1'
        data = open(self.getTestFilePath(fname), "rb").read()
        cd = model.SI_CardData(data)
        p.generateText(cd, sio)
        self.assertEqual(sio.getvalue(), """\

  Runner:  Mihai Ibanescu
  SI-Card: 2005141

  Start   14:31:29  Split    Time 
 1.  51   14:32:36    1:07    1:07
 2.  52   14:34:19    1:43    2:50
 3.  53   14:35:14    0:55    3:45
 4.  54   14:36:41    1:27    5:12
 5.  55   14:40:10    3:29    8:41
 6.  56   14:42:58    2:48   11:29
 7.  57   14:47:33    4:35   16:04
 8.  58   14:49:29    1:56   18:00
 9.  59   14:50:24    0:55   18:55
10.  49   14:52:05    1:41   20:36
11.  61   14:54:22    2:17   22:53
12.  62   14:55:35    1:13   24:06
13.  63   14:57:19    1:44   25:50
  Finish  14:58:05    0:46   26:36

""")  # noqa

        sio = StringIO()
        fname = 'card5-2'
        data = open(self.getTestFilePath(fname), "rb").read()
        cd = model.SI_CardData(data)
        p.generateText(cd, sio)
        self.assertEqual(sio.getvalue(), """\

  SI-Card: 221779

  Start   00:24:43  Split    Time 
 1.  51   00:26:13    1:30    1:30
 2.  52   00:27:44    1:31    3:01
 3.  53   00:28:36    0:52    3:53
 4.  54   00:30:03    1:27    5:20
 5.  55   00:31:56    1:53    7:13
 6.  56   00:33:30    1:34    8:47
 7.  57   00:35:41    2:11   10:58
 8.  58   00:38:18    2:37   13:35
 9.  59   00:39:02    0:44   14:19
10.  49   00:40:19    1:17   15:36
11.  61   00:42:12    1:53   17:29
12.  62   00:43:11    0:59   18:28
13.  63   00:44:11    1:00   19:28
  Finish  00:44:48    0:37   20:05

""")  # noqa

    def testGenerateTextNoStart(self):
        sio = StringIO()
        p = printout.Printout(self.cfg)
        fname = 'card5-3'
        data = open(self.getTestFilePath(fname), "rb").read()
        cd = model.SI_CardData(data)
        p.generateText(cd, sio)
        self.assertEqual(sio.getvalue(), """\

  SI-Card: 348909

  Start   NO-START  Split    Time 
 1. 202   10:56:52   51:50   51:50
 2. 201   11:59:50   62:58  114:48
 3. 204   12:48:37   48:47  163:35
 4. 206   12:58:37   10:00  173:35
  Finish  12:59:15    0:38   XX:XX

""")  # noqa

    def testGenerateTextNoPunchesNoStartNoFinish(self):
        sio = StringIO()
        p = printout.Printout(self.cfg)
        fname = 'card-data-8'

        data = open(self.getTestFilePath(fname), "rb").read()
        cd = model.SI_CardData(data)
        p.generateText(cd, sio)
        self.assertEqual(sio.getvalue(), """\

  Runner:  *** BOK RENTAL ***
  SI-Card: 2005099

  Start   NO-START  Split    Time 
  Finish  NO-FIN   XX:XX   XX:XX

""")  # noqa

    def testGenerateTextNoPunchesNoFinish(self):
        sio = StringIO()
        p = printout.Printout(self.cfg)
        fname = 'card-data-nofinish'

        data = open(self.getTestFilePath(fname), "rb").read()
        cd = model.SI_CardData(data)
        p.generateText(cd, sio)
        self.assertEqual(sio.getvalue(), """\

  Runner:  *** BOK RENTAL ***
  SI-Card: 2005007

  Start   00:08:51  Split    Time 
  Finish  NO-FIN   XX:XX   XX:XX

""")  # noqa


class StandingsTest(ConfigBase):

    def _setupCourseFile(self):
        coursesFilePath = self.cfg.coursesFilePath
        open(coursesFilePath, "w").write("""
Yellow Squash=31,32,33,34,35=
Sprint A =  51, 52, 53, 54, 55, 56, 57, 58, 59, 49, 61, 62, 63=3.3km
Green Beans= 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71, 73, 75, 77, 79 = 6.2km
Invalid 1
Invalid 2=
Brown= 51, 53, 55, 57, 59, 61, 63, 65, 67=doesn't matter
""")

    def testStandings(self):
        self._setupCourseFile()
        dh = datahandler.DataHandler(self.cfg)

        t_empty = model.PunchingRecordTimeOnly()
        t_empty.pth = 0xEE
        data = open(self.getTestFilePath("card5-2"), "rb").read()
        card_no_start = model.SI_CardData(data)
        card_no_start.time_start = t_empty
        card_no_finish = model.SI_CardData(data)
        card_no_finish.time_finish = t_empty
        resultMap = [
            ('card-data-8', "Unable to match course"),
            ('card-data-nofinish', "Unable to match course"),
            (card_no_start, "Disqualified: start not punched"),
            (card_no_finish, "Disqualified: finish not punched"),
            ('pCard-1', "DNF: Green Beans"),
            ('card5-2',
                "Unofficial result on Sprint A: 1 / 1 (0:00 after leader)"),
            ('card-data-1',
                "Unofficial result on Sprint A: 2 / 2 (6:31 after leader)"),
        ]

        for fname, expOut in resultMap:
            if isinstance(fname, str):
                data = open(self.getTestFilePath(fname), "rb").read()
                cd = model.SI_CardData(data)
            else:
                cd = fname
            self.assertEqual(dh.computeStandings(cd), expOut)

        cd.card_id += 1
        cd.name_1 = b'Jean Valjean 1'
        cd.time_finish.time.ptl -= 2
        expOut = "Unofficial result on Sprint A: 2 / 3 (6:29 after leader)"
        self.assertEqual(dh.computeStandings(cd), expOut)

        cd.card_id += 1
        cd.name_1 = b'Jean Valjean 2'
        expOut = "Unofficial result on Sprint A: 2 / 4 (6:29 after leader)"
        self.assertEqual(dh.computeStandings(cd), expOut)

        # Duplicate
        self.assertEqual(dh.computeStandings(cd), expOut)


class CoursesTest(ConfigBase):
    def setUp(self):
        ConfigBase.setUp(self)
        coursesFilePath = self.cfg.coursesFilePath
        open(coursesFilePath, "w").write("""
Yellow Squash=31,32,33,34,35=
Sprint A =  51, 52, 53, 54, 55, 56, 57, 58, 59, 49, 61, 62, 63=3.3km
Green Beans= 51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71, 73, 75, 77, 79 = 6.2km
Invalid 1
Invalid 2=
Brown= 51, 53, 55, 57, 59, 61, 63, 65, 67=doesn't matter
""")  # noqa

    def testCourses(self):
        coursesL = courses.Courses(self.cfg.coursesFilePath)
        # Make sure the invalid entries in the courses file don't show up
        self.assertEqual(len(coursesL.courses), 4)
        self.assertEqual(
            [x.name for x in coursesL.courses],
            ['Green Beans', 'Sprint A', 'Brown', 'Yellow Squash'])
        self.assertEqual(coursesL.match([1, 2, 3, 4, 5]), None)
        self.assertEqual(
            coursesL.match([31, 32, 33, 34, 35]),
            'Yellow Squash')
        self.assertEqual(coursesL.match(
            [51, 53, 55, 57, 59, 61, 63, 65, 67, 69]),
            'Brown')
        self.assertEqual(coursesL.match(
            [51, 53, 55, 57, 59, 61, 63, 65, 67, 69, 71, 73, 75, 77, 79, 81]),
            'Green Beans')
        self.assertEqual(coursesL.match(
            [51, 53, 31, 33, 34, 32, 34, 33, 34, 35]),
            'Yellow Squash')

        self.assertEqual(coursesL.match(
            [51, 52, 53, 54, 55, 56, 57, 58, 59, 49, 61, 62, 63]),
            'Sprint A')

        # Match a punch card
        fname = 'card5-2'
        data = open(self.getTestFilePath(fname), "rb").read()
        cd = model.SI_CardData(data)

        self.assertEqual(coursesL.match(cd), 'Sprint A')
