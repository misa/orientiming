#!/bin/bash

TOOL=/usr/bin/brpapertoollpr_ql700

width=62
for i in $(seq 40); do
  height=$[60+5*i]
  name=otiming_${width}_${height}_mm
  $TOOL -P QL-700 -d $name
  $TOOL -P QL-700 -n $name -w $width -h $height
done
