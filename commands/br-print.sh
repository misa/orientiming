#!/bin/bash

PRINTER="QL-700"

[ $# -ne 1 ] && echo "Usage: $0 <file>" && exit 1

f="$1"
pdfinfo "$f" | grep "^Page size:" |
 sed 's/Page size:[[:blank:]]*\([0-9.]*\)[[:blank:]]*x[[:blank:]]*\([0-9.]*\)[[:blank:]]*.*$/\1 \2/' |
	while read w h; do
	  hh=$(python -c "import math; print(int(math.ceil($h/72*25.4/5 + 3))*5)")
          media=otiming_62_${hh}_mm
          media_id=$(curl http://localhost:631/printers/${PRINTER}.ppd 2>/dev/null | grep '^\*PageSize' | grep ${media} | awk '{print $2}' | cut -d/ -f1)
          lpoptions -p ${PRINTER} -o "media=${media_id}"
	  lpr -P ${PRINTER} ${f}
	done
